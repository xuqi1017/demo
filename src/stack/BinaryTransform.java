package stack;

import java.util.Stack;

/**
 * @Date: 2023/11/15
 * @Author: xuqi
 * @Description:
 */
public class BinaryTransform {

    public static void main(String[] args) {
        // 十进制转其他进制
        System.out.println(transform(1348,8));
    }

    public static String transform(int tenNum ,int bin){
        Stack<Integer> stack = new Stack<>();
        int div=1;
        while (tenNum!=0){
            stack.push(tenNum% bin);
            tenNum = tenNum / bin;
        }
        StringBuilder sb = new StringBuilder();
        while (!stack.empty()){
            sb.append(stack.pop());
        }

        return sb.toString();
    }
}
