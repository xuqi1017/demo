package stack;

import java.util.Arrays;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @Date: 2023/12/11
 * @Author: xuqi
 * @Description:
 */
public class MonotonousStack {

    public static Stack<Integer> increase(int[] arr) {
        Stack<Integer> stack = new Stack<>();
        for (int i : arr) {
            while (!stack.isEmpty() && i < stack.peek()) {
                stack.pop();
            }
            stack.push(i);
        }
        return stack;
    }

    /***
     * 描述：给定一个列表 temperatures，temperatures[i] 表示第 i 天的气温。
     *
     * 要求：输出一个列表，列表上每个位置代表
     * 「如果要观测到更高的气温，至少需要等待的天数」。
     * 如果之后的气温不再升高，则用 0 来代替。
     * @param arr
     */
    public static int[] temperatures(int[] arr) {

        Stack<Integer> stack = new Stack<>();
        int[] result = new int[arr.length];
        // 递减栈，当数据出栈时，此时即将入栈的数为即将出栈的数的下一个更大值
        for (int i = 0; i < arr.length; i++) {
            while (!stack.isEmpty() && arr[i] > arr[stack.peek()]) {
                Integer pop = stack.pop();
                result[pop] = i - pop;
            }
            stack.push(i);
        }
        return result;

    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(temperatures(new int[]{73, 74, 75, 71, 69, 72, 76, 73})));
    }

}
