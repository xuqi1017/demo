package stack;

/**
 * @Date: 2023/11/15
 * @Author: xuqi
 * @Description:
 */
public class Compute {

    public static void main(String[] args) {

    }

    public int[] singleNumber(int[] nums) {
        if(nums.length==2){
            return nums;
        }
        int eor= 0;
        for(int num:nums){
            eor=eor^num;
        }
        int rightOne = eor & (~eor +1);
        int eorP= 0;
        for(int num:nums){
            if((num&rightOne)==0){
                eorP=eorP^num;
            }
        }
        int a = eorP;
        int b = eor^eorP;
        return new int[]{a,b};
    }
}
