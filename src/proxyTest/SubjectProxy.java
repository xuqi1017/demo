package proxyTest;


public class SubjectProxy implements Subject
{
    Subject subimpl = new RealSubject();
    @Override
    public void Hello()
    {
        subimpl.Hello();
    }

    public static void main(String[] args) {

        Subject subject = new SubjectProxy();
        subject.Hello();
    }


}
