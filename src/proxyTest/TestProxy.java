package proxyTest;

public class TestProxy {
    public static void main(String[] args) {
        ProxyHandler proxyHandler = new ProxyHandler();
        Subject sb = (Subject) proxyHandler.bind(new RealSubject());
        sb.Hello();
    }
}
