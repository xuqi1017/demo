package linkedList;

import leetCode.LinkedNode;


/**
 * @Date: 2023/12/18
 * @Author: xuqi
 * @Description:
 */
public class ReverseBetween {
    public static void main(String[] args) {
        LinkedNode head = LinkedNode.initLink(new int[]{1, 2, 3, 4, 5});
        LinkedNode res = reverseBetween(head, 2, 4);
        LinkedNode.print(res);
    }

    public static LinkedNode reverseBetween(LinkedNode head, int left, int right) {
        LinkedNode voidNode = new LinkedNode(-1);
        voidNode.next = head;

        LinkedNode pre = voidNode;
        for (int i = 0; i < left - 1; i++) {
            pre = pre.next;
        }
        LinkedNode leftNode = pre.next;
        LinkedNode rightNode = leftNode;

        for (int i = 0; i < right - left; i++) {
            rightNode = rightNode.next;
        }
        LinkedNode rightNextNode = rightNode.next;
        pre.next =null;
        rightNode.next = null;

        pre.next = reverse(leftNode);
        leftNode.next=rightNextNode;
        return voidNode.next;

    }

    public static LinkedNode reverse(LinkedNode head){
        LinkedNode pre = null;
        LinkedNode next = head;
        while (head!=null){
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }
}
