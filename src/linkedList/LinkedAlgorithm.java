package linkedList;

import leetCode.LinkedNode;

/**
 * @Date: 2023/10/24
 * @Author: xuqi
 * @Description:
 */
public class LinkedAlgorithm {

    public static void main(String[] args) {
        LinkedNode head = LinkedNode.initLink(new int[]{1, 5, 3, 4, 2, 6, 7, 30, 9});
        LinkedNode.print(head);
        LinkedNode linkedNode = reverseLoop(head);
        //LinkedNode linkedNode = reverseRecursion(head);
        //LinkedNode linkedNode = exchange(head, 2, 5);
        //LinkedNode linkedNode = insertSort(head);
        //LinkedNode linkedNode = mergeSort(head);
        LinkedNode.print(linkedNode);
    }

    // 翻转链表循环
    public static LinkedNode reverseLoop(LinkedNode head) {
        LinkedNode pre = null, next;
        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    // 判断是否相交
    public static LinkedNode checkIntersect(LinkedNode head1, LinkedNode head2) {
        if (head1 == null || head2 == null) {
            return null;
        }
        LinkedNode circle1 = checkCircle(head1);
        LinkedNode circle2 = checkCircle(head2);
        if (head1 == head2) {
            return head1;
        }
        if (circle1 != null && circle2 != null) {
            // 三种情况，不相交，相交在环上，不相交在环视
            if (circle1 == circle2) {
                return intersect(head1, head2, circle2);
            }
            LinkedNode loop1 = circle1.next;
            while (loop1 != circle1) {
                if (loop1 == circle2) {
                    return circle2;
                }
                loop1 = loop1.next;
            }
            return null;
        }
        if (circle1 == null && circle2 == null) {
            return intersect(head1, head2, null);
        }
        return null;

    }

    private static LinkedNode intersect(LinkedNode head1, LinkedNode head2, LinkedNode circe) {
        LinkedNode cur1 = head1, cur2 = head2;
        int error = 0;
        while (cur1.next != circe) {
            cur1 = cur1.next;
            error++;
        }
        while (cur2.next != circe) {
            cur2 = cur2.next;
            error--;
        }
        if (cur1 != cur2) {
            return null;
        }
        cur1 = error > 0 ? head1 : head2;
        cur2 = error > 0 ? head2 : head1;
        // 1 是长的
        error = Math.abs(error);
        //1 先走
        for (int i = 0; i < error; i++) {
            cur1 = cur1.next;
        }
        while (cur1 != cur2) {
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        return cur1;
    }

    // 判断链表有没有环，如果有返回入换节点。
    public static LinkedNode checkCircle(LinkedNode head) {
        if (head == null || head.next == null) {
            return null;
        }
        LinkedNode fast = head, slow = head;
        boolean flag = false;
        while (fast != null) {
            if (flag) {
                fast = fast.next;
            } else {
                fast = fast.next.next;
            }
            slow = slow.next;
            if (fast == slow) {
                if (flag) {
                    return fast;
                }
                flag = true;
                fast = head;
            }
        }
        return null;
    }

    //翻转链表递归
    public static LinkedNode reverseRecursion(LinkedNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        LinkedNode tail = reverseRecursion(head.next);
        head.next.next = head;
        head.next = null;
        return tail;
    }

    public static LinkedNode exchange(LinkedNode head, int i, int j) {
        // j>i>0
        LinkedNode ipNode = null, iNode = null, iNext = null, jpNode = null, jNode = null, jNext = null, temp = null;
        int num = 0;
        temp = head;
        while (temp != null) {
            if (i == num + 1) {
                ipNode = temp;
                iNode = temp.next;
                if (iNode != null) {
                    iNext = temp.next.next;
                }
            }
            if (j == num + 1) {
                jpNode = temp;
                jNode = temp.next;
                if (jNode != null) {
                    jNext = temp.next.next;
                }
            }
            temp = temp.next;
            num++;
        }
        ipNode.next = jNode;
        jNode.next = iNext;
        jpNode.next = iNode;
        iNode.next = jNext;
        return head;

    }

    public static LinkedNode insertSort(LinkedNode head) {
        LinkedNode headPre = new LinkedNode(Integer.MIN_VALUE);
        headPre.next = head;
        LinkedNode cur = head.next;
        LinkedNode sortedLast = head;
        while (cur != null) {
            if (cur.val >= sortedLast.val) {
                sortedLast = cur;
            } else {
                LinkedNode pre = headPre;
                while (pre.next.val <= cur.val) {
                    pre = pre.next;
                }
                sortedLast.next = cur.next;
                cur.next = pre.next;
                pre.next = cur;
            }
            cur = sortedLast.next;
        }
        return headPre.next;
    }

    public static LinkedNode mergeSort(LinkedNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        LinkedNode linkedNode = halfSpilt(head);
        return merge(mergeSort(head), mergeSort(linkedNode));
    }

    private static LinkedNode merge(LinkedNode node1, LinkedNode node2) {
        LinkedNode newNode = new LinkedNode(0);
        LinkedNode last = newNode;
        while (node1 != null && node2 != null) {
            if (node1.val < node2.val) {
                last.next = node1;
                last = last.next;
                node1 = node1.next;
            } else {
                last.next = node2;
                last = last.next;
                node2 = node2.next;
            }
        }
        while (node1 != null) {
            last.next = node1;
            last = last.next;
            node1 = node1.next;
        }
        while (node2 != null) {
            last.next = node2;
            last = last.next;
            node2 = node2.next;
        }
        return newNode.next;
    }

    private static LinkedNode halfSpilt(LinkedNode head) {
        LinkedNode slow = head;
        LinkedNode slowPre = null;
        while (head != null && head.next != null) {
            head = head.next.next;
            slowPre = slow;
            slow = slow.next;
        }
        slowPre.next = null;
        return slow;
    }


}
