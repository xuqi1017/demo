package linkedList;

import leetCode.LinkedNode;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

/**
 * @Date: 2023/11/23
 * @Author: xuqi
 * @Description:
 */
public class RandomLinkedNode extends LinkedNode {
    RandomLinkedNode random;
    RandomLinkedNode next;

    public RandomLinkedNode(int x) {
        super(x);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RandomLinkedNode that = (RandomLinkedNode) o;
        return Objects.equals(random, that.random);
    }

    @Override
    public int hashCode() {
        return Objects.hash(random);
    }

    public static RandomLinkedNode deepCopy(RandomLinkedNode head) {
        if (head == null) {
            return head;
        }
        Map<RandomLinkedNode, RandomLinkedNode> map = new HashMap<>();
        RandomLinkedNode temp = head;
        while (temp != null) {
            map.put(temp, new RandomLinkedNode(temp.val));
            temp = temp.next;
        }
        temp = head;
        while (temp != null) {
            map.get(temp).next = temp.next;
            map.get(temp).random = temp.random;
            temp = temp.next;
        }
        return map.get(head);

    }

    public static RandomLinkedNode deepCopyOptimize(RandomLinkedNode head) {
        // 在源节点的下一个节点新建克隆节点
        if (head == null) {
            return head;
        }
        RandomLinkedNode cur = head;
        while (cur != null) {
            RandomLinkedNode copyNode = new RandomLinkedNode(cur.val);
            copyNode.next = cur.next;
            cur.next = copyNode;
            cur = copyNode.next;
        }
        cur = head;
        RandomLinkedNode newHead = head.next;
        while (cur != null) {
            RandomLinkedNode copyNode = cur.next;
            copyNode.random = cur.random == null ? null : cur.random.next;
            cur.next = copyNode.next;
            copyNode.next = copyNode.next==null?null:copyNode.next.next;
            cur = cur.next;
        }
        return newHead;
    }
}
