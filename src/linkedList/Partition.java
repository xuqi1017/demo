package linkedList;

import leetCode.LinkedNode;

/**
 * @Date: 2023/12/18
 * @Author: xuqi
 * @Description:
 */
public class Partition {
    public static void main(String[] args) {
        LinkedNode head = LinkedNode.initLink(new int[]{1,4,3,2,5,2});
        LinkedNode res = partition(head, 3);
        LinkedNode.print(res);
    }
    public static LinkedNode partition(LinkedNode head, int x) {
        LinkedNode small = new LinkedNode(0);
        LinkedNode smallHead = small;
        LinkedNode large = new LinkedNode(0);
        LinkedNode largeHead = large;

        while (head!=null){
            if (head.val<x){
                small.next = head;
                small = small.next;
            }else {
                large.next = head;
                large = large.next;
            }
            head= head.next;
        }
        large.next = null;
        small.next = largeHead.next;
        return smallHead.next;
    }
}
