package linkedList;

import huawei.Exam;
import leetCode.LinkedNode;

/**
 * @Date: 2023/11/22
 * @Author: xuqi
 * @Description:
 */
public class RepeatText {
    public static boolean repeatText(LinkedNode head) {
        if (head == null || head.next == null) {
            return true;
        }
        LinkedNode slow = head;
        LinkedNode fast = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        LinkedNode reverse = LinkedAlgorithm.reverseLoop(slow);
        while (reverse != null) {
            if (reverse.val == head.val) {
                reverse = reverse.next;
                head = head.next;
            } else {
                return false;
            }
        }
        // TODO 把原来的结构调过来，
        return true;
    }

    public static void main(String[] args) {

        LinkedNode head = LinkedNode.initLink(new int[]{1, 1,1,2,3,4,35346,245,6,345,6,34,57,4,56,2,});
        LinkedNode.print( partition(head,24536345));
    }

    public static LinkedNode partition(LinkedNode head, int val) {
        if (head == null || head.next == null) {
            return head;
        }
        LinkedNode sh = null, st = null, eh = null, et = null, bh = null, bt = null, cur;
        cur = head;
        while (cur != null) {
            if (cur.val < val) {
                if (sh == null) {
                    sh = cur;
                    st = cur;
                } else {
                    st.next = cur;
                    st = st.next;
                }
            } else if (cur.val == val) {
                if (eh == null) {
                    eh = cur;
                    et = cur;
                } else {
                    et.next = cur;
                    et = et.next;
                }
            } else {
                if (bh == null) {
                    bh = cur;
                    bt = cur;
                } else {
                    bt.next = cur;
                    bt = bt.next;
                }
            }
            cur = cur.next;
        }
        if (bt != null) {
            bt.next = null;
        }
        if (sh == null && eh == null) {
            return bh;
        }
        if (bh == null && eh == null) {
            return sh;
        }
        if (sh == null && bh == null) {
            return eh;
        }
        if (sh == null) {
            et.next = bh;
            return eh;
        }
        if (eh == null) {
            st.next = bh;
            return sh;
        }

        st.next = eh;
        et.next = bh;
        return sh;


    }

}
