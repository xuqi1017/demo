package aqs;

import java.util.concurrent.CountDownLatch;

/**
 * @Date: 2023/11/30
 * @Author: xuqi
 * @Description:
 */
public class TestCountDown {
    public static void main(String[] args) {
        CountDownLatch count = new CountDownLatch(2);
        Thread t1 = new Thread(()->{
            try {
                System.out.println("t1Start");
                count.await();
                Thread.sleep(2000);
                System.out.println("t1End");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(()->{
            try {
                System.out.println("t2Start");
                count.countDown();
                Thread.sleep(2000);
                System.out.println("t2End");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t3 = new Thread(()->{
            try {
                System.out.println("t3Start");
                count.countDown();
                Thread.sleep(2000);
                System.out.println("t3End");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t3.start();
    }
}
