package aqs;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * @Date: 2023/11/30
 * @Author: xuqi
 * @Description:
 */
public class MyLock {
    private Sync sync;

    public MyLock(){
        sync = new Sync();
    }
    public void  lock(){
        sync.tryAcquire(1);
    }
    public void unlock(){
        sync.tryRelease(1);
    }

    public void showLockThread(){
        sync.showLockThread();
    }

    class Sync extends AbstractQueuedSynchronizer {
        @Override
        protected boolean tryAcquire(int acquire) {
            final Thread current = Thread.currentThread();
            int state = getState();
            if (state == 0) {
                if (compareAndSetState(0, acquire)) {
                    setExclusiveOwnerThread(current);
                    return true;
                }
            } else if (current == getExclusiveOwnerThread()) {
                int nextState = state + acquire;
                setState(nextState);
                return true;
            }
            return false;
        }

        @Override
        protected boolean tryRelease(int releases) {
            int c = getState() - releases;
            if (Thread.currentThread() != getExclusiveOwnerThread()) {
                throw new IllegalMonitorStateException();
            }
            boolean res = false;
            if (c == 0) {
                setExclusiveOwnerThread(null);
                res=true;
            }
            setState(c);
            return res;
        }

        public void showLockThread() {
            if (getExclusiveOwnerThread()!=null){
                System.out.println("当前线程："+getExclusiveOwnerThread().getName());
            }else {
                System.out.println("无当前线程");

            }
        }
    }


    public static void main(String[] args) throws InterruptedException {
        final MyLock myLock =new MyLock();
        Thread t1 = new Thread(()->{
            try {
                System.out.println("t1Start");
                myLock.lock();
                System.out.println("t1getLock");

                Thread.sleep(5000);
                System.out.println("t1End");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                myLock.unlock();
            }
        });
        t1.setName("T1");

        Thread t2 = new Thread(()->{
            try {
                System.out.println("t2Start");
                myLock.lock();
                System.out.println("t2getLock");
                System.out.println("t2End");
            } finally {
                myLock.unlock();
            }
        });
        t2.setName("T2");
        t1.start();
        t2.start();
        Thread.sleep(2000);
        myLock.showLockThread();
        t1.join();
        t2.join();
    }
}
