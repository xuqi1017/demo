package huawei;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.locks.LockSupport;


public class Exam3 {
    public static void main(String[] args) {
        LockSupport.park();
        Scanner sc = new Scanner(System.in);
        int guai = sc.nextInt();
        int qing = sc.nextInt();
        String s1 = sc.nextLine();
        int hang = 0;
        int lie = 0;
        int si = 0, sj = 0, ti = 0, tj = 0;
        List<String> input = new ArrayList<>();
        if (s1.contains(" ")) {
            String[] split = s1.split(" ");
            hang = Integer.valueOf(split[0]);
            lie = Integer.valueOf(split[1]);
        } else {
            lie = s1.length();
            input.add(s1);
        }
        while (sc.hasNextLine()){
            input.add(sc.nextLine());
        }
        if (hang==0){
            hang = input.size();
        }
        int[][]  map = new int[hang][lie];

        for (int i = 0; i < hang; i++) {

            char[] chars = input.get(i).toCharArray();
            for (int i1 = 0; i1 < lie; i1++) {
                if (chars[i1] == '.') {
                    map[i][i1] = 0;
                } else if (chars[i1] == '*') {
                    map[i][i1] = 1;
                } else if (chars[i1] == 'S') {
                    map[i][i1] = 0;
                    si = i;
                    sj = i1;
                } else {
                    map[i][i1] = 0;
                    ti = i;
                    tj = i1;
                }
            }
        }
        if (hang == 0 || lie == 0) {
            System.out.println("NO");
            return;
        }
        // s-jia
        //t-com
        // 0 通过 1路障

        boolean dfs = dfs(map, si, sj, ti, tj, 0, guai + 1, qing, hang, lie);
        if (dfs) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }

    }

    public static boolean dfs(int[][] map, int x, int y, int ti, int tj, int d, int guai, int qing, int hang, int lie) {
        if (x == ti && y == tj) {
            return true;
        }
        int qing1 = 0;
        if (map[x][y] == 1) {
            if (qing > 0) {
                qing1 = qing-1;

            } else {
                return false;
            }
        }
        boolean f = false;
        //wang zuo 1
        if (d != 2 && y > 0) {
            if (d != 1) {
                //guai
                if (guai > 0) {
                    f = f || dfs(map, x, y - 1, ti, tj, 1, guai - 1, qing1, hang, lie);
                }
            } else {
                f = f || dfs(map, x, y - 1, ti, tj, 1, guai, qing1, hang, lie);
            }
        }
        //you 2
        if (d != 1 && y < lie - 1) {
            if (d != 2) {
                if (guai > 0) {
                    f = f || dfs(map, x, y + 1, ti, tj, 2, guai - 1, qing1, hang, lie);
                }
            } else {
                f = f || dfs(map, x, y + 1, ti, tj, 2, guai, qing1, hang, lie);
            }
        }
        //shang 3
        if (d != 4 && x > 0) {
            if (d != 3) {
                if (guai > 0) {
                    f = f || dfs(map, x - 1, y, ti, tj, 3, guai - 1, qing1, hang, lie);
                }
            } else {
                f = f || dfs(map, x - 1, y, ti, tj, 3, guai, qing1, hang, lie);
            }
        }
        //xia 4
        if (d != 3 && x < hang - 1) {
            if (d != 2) {
                if (guai > 0) {

                    f = f || dfs(map, x + 1, y, ti, tj, 4, guai - 1, qing1, hang, lie);
                }
            } else {
                f = f || dfs(map, x + 1, y, ti, tj, 4, guai, qing1, hang, lie);
            }

        }
        return f;
    }


}
