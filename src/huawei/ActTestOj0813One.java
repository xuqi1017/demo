package huawei;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 题目描述：某小区的停车数据包含车牌号、停车日期、停车时长，给出的数据保证是同一年，车牌号由T和5位数字组成，确保了车牌号是合理的输入。
 * 需要统计某个月前5名的停车车牌，统计规则如下：
 * 1、优先按停车的总时长
 * 排序，总时长大的排在前面
 * 2、如果时长相同，则优先按停车次数排序，总停车次数大的排在前面
 * 3、如果总时长和停车次数都相同，则按照车牌号的字典序排序
 */
public class ActTestOj0813One {

    private static List<String> getTopCars(int i, List<Record> records) {
        String m = i > 9 ? i + "" : "0" + i;
        Map<String, List<Record>> collect = records.stream().filter(r -> r.getDate().startsWith("2021-" + m))
                .collect(Collectors.groupingBy(Record::getChepai));
        List<Record> aggregateList = collect.values().stream().map(r-> aggregate(r)).collect(Collectors.toList());
        return aggregateList.stream().sorted().map(Record::getChepai).collect(Collectors.toList());
    }

    public static Record aggregate(List<Record> records) {
        if (records == null || records.size() == 0)
            return null;
        Record record1 = new Record("", "", 0);
        int time = 0;
        int num = 0;
        for (Record record : records) {
            num++;
            time += record.getTime();
        }
        record1.setChepai(records.get(0).getChepai());
        record1.setNum(num);
        record1.setTime(time);
        return record1;
    }


    public static void main(String[] args) {
        List<Record> records = new ArrayList<>();
        ActTestOj0813One.Record one = new ActTestOj0813One.Record("T6563", "2021-01-31", 50);
        ActTestOj0813One.Record two = new ActTestOj0813One.Record("T2345", "2021-05-31", 120);
        ActTestOj0813One.Record three = new ActTestOj0813One.Record("T6563", "2021-05-06", 8);
        ActTestOj0813One.Record four = new ActTestOj0813One.Record("T2311", "2021-05-08", 12);
        ActTestOj0813One.Record five = new ActTestOj0813One.Record("T2311", "2021-05-20", 108);
        ActTestOj0813One.Record six = new ActTestOj0813One.Record("T6563", "2021-05-30", 80);

        records.add(one);
        records.add(two);
        records.add(three);
        records.add(four);
        records.add(five);
        records.add(six);
        List<String> rel = getTopCars(5, records);
        for (String y : rel) {
            System.out.println(y);
        }
    }

    public static class Record implements Comparable<Record> {
        @Override
        public int compareTo(Record o) {
            if (o.getTime() != this.getTime()) {
                return  o.getTime()-this.getTime() ;
            }
            if (o.getNum() != this.getNum()) {
                return o.getNum()-this.getNum() ;
            }
            return this.getChepai().compareTo(o.getChepai());

        }

        public Integer getNum() {
            return num;
        }

        public void setNum(Integer num) {
            this.num = num;
        }

        private String chepai;
        private Integer num;

        public String getChepai() {
            return chepai;
        }

        public void setChepai(String chepai) {
            this.chepai = chepai;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getTime() {
            return time;
        }

        public void setTime(Integer time) {
            this.time = time;
        }

        public Record(String chepai, String date, Integer time) {
            this.chepai = chepai;
            this.date = date;
            this.time = time;
        }

        private String date;
        private Integer time;


    }




}
