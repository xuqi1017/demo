package huawei;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//00100 4
//00000 4 -1
//00100 1 12309
//33218 3 00000
//12309 2 33218
public class Exam {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String chushi = sc.nextLine();
        if (chushi.equals("")||chushi==null){
            System.out.println(0);
            return;
        }
        String[] s2 = chushi.split(" ");
        String head = s2[0];

        Integer num = Integer.valueOf(s2[1]);

        Map<String, Node> map = new HashMap<>();
        for(int i = 0; i < num; i++){
            String s = sc.nextLine();
            String[] s1 = s.split(" ");
            Node node = new Node();
            node.address=s1[0];
            node.val=Integer.valueOf(s1[1]);
            node.next=s1[2];
            map.put(node.address,node);
        }
        int n = num/2;

        int res = 0;
        for (int i = 0; i <= n; i++) {
            Node node = map.get(head);
            if (node==null){
                System.out.println(0);
                return;
            }
            res = node.val;
            head = node.next;
        }
        System.out.println(res);
    }
    public static  class Node{
        public String address;
         public Integer val;
        public String next;
    }
}
