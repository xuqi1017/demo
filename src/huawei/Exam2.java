package huawei;

import java.util.*;

public class Exam2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String mapper = sc.nextLine();
        String letter = sc.nextLine();
        String[] m = mapper.split(" ");
        String[] l = letter.split(" ");
        HashMap<String,Integer> set = new HashMap<>();
        for (int i = 0; i < m.length; i++) {
            String sort = sort(m[i]);
            Integer integer = set.get(sort);
            if (integer==null){
                set.put(sort,1);
            }else {
                integer++;
            }
        }
        for (String s : l) {
            Integer integer = set.get(sort(s));
            if (integer==null){
                System.out.println("false");
                return;
            }else {
                if (integer==0){
                    System.out.println("false");
                    return;
                }else {
                    integer--;
                }
            }
        }
        System.out.println("true");



    }

    public static String sort (String word){
        char[] chars = word.toCharArray();
        Arrays.sort(chars);
        String s = String.copyValueOf(chars);
        return s;
    }
}
