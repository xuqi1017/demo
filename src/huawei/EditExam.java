package huawei;

public class EditExam {


    public static void main(String[] args) {
        boolean edit = edit("asfasf", "asfas1s");
        System.out.println(edit);
    }

    public static boolean edit(String s1, String s2) {
        int length1 = s1.length();
        int length2 = s2.length();
        if (length1 == length2) {
            int d = 0;
            for (int i = 0; i < length1; i++) {
                if (s1.charAt(i) != s2.charAt(i)) {
                    d++;
                }
                if (d > 1) {
                    return false;
                }
            }
            return true;
        } else if (length1 - length2 == 1) {
            return check(s1, s2, length1);
        } else if (length2 - length1 == 1) {
            return check(s2, s1, length2);
        }
        return false;

    }

    public static boolean check(String chang, String duan, int lengthChang) {
        int i = 0, j = 0;
        for (; i < lengthChang - 1 && j < lengthChang; j++) {
            if (chang.charAt(j) == duan.charAt(i)) {
                i++;
            }
        }
        return j == i - 1;

    }

}
