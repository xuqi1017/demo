package hw;

import java.util.*;

public class HJ26 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String s = in.nextLine();
        List<List<Character>> button = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            List<Character> letterList = new ArrayList<>();
            button.add(letterList);
        }
        for (char c : s.toCharArray()) {
            if (Character.isLetter(c)){
                //System.out.println(Character.toUpperCase(c) -65);
                List<Character> characters = button.get(Character.toUpperCase(c) -65);
                characters.add(c);
            }
        }
        Queue<Character> queue = new LinkedList<>();
        for (int i = 0; i < 26; i++) {
            for (int i1 = 0; i1 < button.get(i).size(); i1++) {
                queue.add(button.get(i).get(i1));
            }
        }
        StringBuilder sb = new StringBuilder();
        for (char c : s.toCharArray()) {
            if (Character.isLetter(c)){
                sb.append(queue.poll());
            }else {
                sb.append(c);
            }
        }
        System.out.println(sb.toString());

    }
}
