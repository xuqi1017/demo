package hw;

import java.util.Locale;
import java.util.Scanner;

public class HJ2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine().toLowerCase();
        String target = in.next().toLowerCase();
        char[] chars = target.toCharArray();
        char aChar = chars[0];
        int num = 0;
        for (char c : s.toCharArray()) {
            if (c==aChar)num++;
        }
        System.out.println(num);

    }
}
