package hw;

import java.util.Stack;

public class HWInterview1 {

    /**
     * 题目描述
     * Solo小学一年级的时候做数学题很莫名奇妙，经常把算术表达式加上很多空格（如：7+ 31  -2）
     * ，让老师很是头大，于是老师决定雇用你编写一个程序来独立计算Solo的答案。Can you help the teacher?
     * <p>
     * 解答要求
     * 时间限制：5000ms, 内存限制：100MB
     * 输入
     * 输入只有一行，即一个长度不超过100的字符串S，表示Solo的算术表达式，
     * 且S只包含数字和”+”、”-”两种运算符，以及Solo加上的一大堆空格（我们保证输入都是合法的）。
     * 注意：S中不一定包含运算符，且我们保证S中不会出现大于100000的数。
     * <p>
     * 输出
     * 输出表达式的运算结果。
     * <p>
     * 样例
     * 输入样例 1 复制
     * <p>
     * 1+2 + 3 +   4
     * 输出样例 1
     * <p>
     * 10
     * 提示样例 1
     * <p>
     * <p>
     * 提示
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(compute(" 1+-2+1--1 "));

        System.out.println(compute(" 1+2 - 3 +   4"));
        System.out.println(compute("11-    4"));
        System.out.println(compute("11-22 - 33 +   4   "));
        System.out.println(compute("    "));
        System.out.println(compute("90-232-32--23-23-253-235-    "));
        System.out.println(compute("  --  "));
        System.out.println(compute("9 0-232-3 2--23 +-23- 25 3-235-    "));

    }

    public static int compute(String s) {
        if (s.length()<1){
            return 0;
        }
        Stack<Integer> stack = new Stack<Integer>();
        boolean addFlag = true;
        int curNum = 0;
        // 积累当前数字，当遇到计算符号，入栈当前数字，积累之后的数子
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                continue;
            }
            if (c == '+') {
                curNum = addFlag ? curNum : -1 * curNum;
                stack.push(new Integer(curNum));
                curNum = 0;
                addFlag = true;

            } else if (c == '-') {
                curNum = addFlag ? curNum : -1 * curNum;
                stack.push(new Integer(curNum));
                curNum = 0;
                addFlag = false;

            } else {
                curNum = curNum * 10 + (Integer.valueOf(c)-48);
            }
        }
        curNum = addFlag ? curNum : -1 * curNum;
        stack.push(new Integer(curNum));
        curNum = 0;
        for (Integer integer : stack) {
            curNum += integer;
        }
        return curNum;
    }
}
