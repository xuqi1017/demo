package hw;

import java.util.Scanner;

public class HJ24 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        int num = in.nextInt();
        int[] high = new int[num];
        for (int i1 = 0; i1 < num; i1++) {
            high[i1]=in.nextInt();
        }
        int[] left = new int[num];
        int[] right = new int[num];
        int[] res = new int[num];
        left[0] = 1;            //最左边的数设为1
        right[num - 1] = 1;        //最右边的数设为1
        //计算每个位置左侧的最长递增
        for (int i = 0; i < num; i++) {
            left[i] = 1;
            for (int j = 0; j < i; j++) {
                if (high[i] > high[j]) {   //动态规划
                    left[i] = Math.max(left[j] + 1, left[i]);
                }
            }
        }
        //计算每个位置右侧的最长递减
        for (int i = num - 1; i >= 0; i--) {
            right[i] = 1;
            for (int j = num - 1; j > i; j--) {
                if (high[i] > high[j]) {   //动态规划
                    right[i] = Math.max(right[i], right[j] + 1);
                }
            }
        }
        // 记录每个位置的值
        int[] result = new int[num];
        int max = 1;
        for (int i = 0; i < num; i++) {
            //位置 i计算了两次 所以需要－1
            result[i] = left[i] + right[i] - 1; //两个都包含本身
            max = Math.max(result[i],max);

        }

        System.out.println(num - max);
    }
}
