package hw;

import java.util.Scanner;

public class HJ39 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String zi = in.nextLine();
        String ip1 = in.nextLine();
        String ip2 = in.nextLine();
        boolean flag = true;
        if (check(zi)) {
            String[] split = zi.split("\\.");
            for (String s : split) {
                if (!Integer.valueOf(s).equals(0) && !Integer.valueOf(s).equals(255) ) {
                    flag = false;
                    break;
                }
            }
        }else {
            flag = false;
        }
        ThreadLocal t = new ThreadLocal();
        t.remove();
        if (flag  && check(ip1) && check(ip2)) {
            String[] zis = zi.split("\\.");
            String[] ip1s = ip1.split("\\.");
            String[] ip2s = ip2.split("\\.");
            for (int i = 0; i < zis.length; i++) {
                int i1 = Integer.valueOf(ip1s[i]) & Integer.valueOf(zis[i]);
                int i2 = Integer.valueOf(ip2s[i]) & Integer.valueOf(zis[i]);
                if (i1 != i2) {
                    flag = false;
                    System.out.println(2);
                    break;
                }
            }
            if (flag){
                System.out.println(0);
            }
        } else {
            System.out.println(1);
        }

    }

    private static boolean check(String ip) {
        String[] split = ip.split("\\.");
        if (split.length != 4) {
            return false;
        }
        for (String s : split) {
            if (Integer.valueOf(s) < 0 || Integer.valueOf(s) > 255) {
                return false;
            }
        }
        return true;
    }
}
