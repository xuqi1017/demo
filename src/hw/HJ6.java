package hw;

import java.io.IOException;
import java.util.Scanner;

public class HJ6 {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
         int val = in.nextInt();
        StringBuilder sb= new StringBuilder();
        for (int i = 2; i*i <=val ; i++) {
            if (val%i==0){
                sb.append(i).append(" ");
                val=val/i;
                i--;

            }
        }
        sb.append(val).append(" ");
        System.out.println(sb);
    }
}
