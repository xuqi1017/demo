package hw;

import java.util.Scanner;

public class HJ43 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int hang = in.nextInt();
        int lie = in.nextInt();
        int[][] num = new int[hang][lie];
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < hang; i++) {
            for (int i1 = 0; i1 < lie; i1++) {
                num[i][i1] = in.nextInt();
            }
        }

        dfs(num,hang,lie,0,0,sb,1);
        System.out.println(sb);
    }

    private static boolean dfs(int[][] num, int hang, int lie, int x, int y,StringBuilder sb,int direct) {
        if (x==-1||y==-1||x==hang||y==lie){
            return false;
        }
        if (x==hang-1&&y==lie-1){
            sb.insert(0,"("+x+","+y+")\n");
            return true;
        }

        //left
        if (x<hang-1&&num[x+1][y]==0&&direct!=4){
            if (dfs(num, hang, lie, x+1, y,sb,1)){
                sb.insert(0,"("+x+","+y+")\n");
                return true;
            }
        }

        //left
        if (y<lie-1&&num[x][y+1]==0&&direct!=3){
            if (dfs(num, hang, lie, x, y+1,sb,2)){
                sb.insert(0,"("+x+","+y+")\n");
                return true;
            }
        }

        //left
        if (y>0&&num[x][y-1]==0&&direct!=2){
            if (dfs(num, hang, lie, x, y-1,sb,3)){
                sb.insert(0,"("+x+","+y+")\n");
                return true;
            }
        }

        //left
        if (x>0&&num[x-1][y]==0&&direct!=1){
            if (dfs(num, hang, lie, x-1, y,sb,4)){
                sb.insert(0,"("+x+","+y+")\n");
                return true;
            }
        }
        return false;
    }

}
