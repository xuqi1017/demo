package hw;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class HJ17 {

//A10;S20;W10;D30;X;A1A;B10A11;;A10;
//10,-10
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        String[] split = s.split(";");
        int x=0;
        int y=0;
        for (String s1 : split) {
            if (check(s1)){
                if (s1.startsWith("A")){
                    Integer integer = Integer.valueOf(s1.substring(1));
                    x-=integer;
                }else if (s1.startsWith("S")){
                    Integer integer = Integer.valueOf(s1.substring(1));
                    y-=integer;
                }else if (s1.startsWith("W")){
                    Integer integer = Integer.valueOf(s1.substring(1));
                    y+=integer;
                }else if (s1.startsWith("D")){
                    Integer integer = Integer.valueOf(s1.substring(1));
                    x+=integer;
                }
            }
        }
        System.out.println(x+","+y);

    }

    static Set<String> direction = new HashSet<>();

    static {
        direction.add("W");
        direction.add("D");
        direction.add("S");
        direction.add("A");

    }

    private static boolean check(String s1) {
        if (s1==null||s1.isEmpty())return false;
        String substring = s1.substring(0, 1);
        if (!direction.contains(substring))return false;
        String substring1 = s1.substring(1);
        try{
            Integer integer = Integer.valueOf(substring1);
            System.out.println(integer);
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
