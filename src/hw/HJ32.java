package hw;

import java.util.Scanner;

public class HJ32 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        int length = s.toCharArray().length;
        int res =1;
        for (int i = 0; i < length-1; i++) {
            for (int j = length-1; j > i; j--) {
                if(check(s,i,j)){
                    res=Math.max(res,j-i+1);
                }
            }
        }
        System.out.println(res);
    }

    private static boolean check(String s, int i, int j) {
        if (i==j||i==j+1)return true;
        if (s.charAt(i)==s.charAt(j)){
            return check(s,i+1,j-1);
        }
        return false;
    }

}
