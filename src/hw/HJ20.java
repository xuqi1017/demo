package hw;

import java.util.Scanner;

public class HJ20 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            String s = in.nextLine();
            if (checkLength(s)&&fourCondition(s)&&hasNotRepeat(s)){
                System.out.println("OK");
            }else {
                System.out.println("NG");
            }
        }
    }

    private static boolean fourCondition(String s) {
        if (hasLow(s)+hasHigh(s)+hasNum(s)+hasOther(s)>2){
            return true;
        }
        return false;
    }

    private static int hasOther(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isLetterOrDigit(s.charAt(i)))return 1;
        }
        return 0;
    }

    private static int hasNum(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (Character.isDigit(s.charAt(i)))return 1;
        }
        return 0;

    }

    private static int hasHigh(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (Character.isUpperCase(s.charAt(i)))return 1;
        }
        return 0;
    }

    private static int hasLow(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (Character.isLowerCase(s.charAt(i)))return 1;
        }
        return 0;
    }

    private static boolean hasNotRepeat(String s) {
        for (int i = 0; i < s.length()-3; i++) {
            for (int j = i+1; j < s.length()-3; j++) {
                if (s.charAt(i)==s.charAt(j)&&s.charAt(i+1)==s.charAt(j+1)&&s.charAt(i+2)==s.charAt(j+2))
                    return false;
            }
        }
        return true;
    }

    private static boolean checkLength(String s) {
        if (s.length()>8){
            return true;
        }
        return false;
    }
}
