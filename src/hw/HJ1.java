package hw;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class HJ1 {

    // 注意类名必须为 Main, 不要有任何 package xxx 信息
        public static void main(String[] args) throws IOException {
            InputStream inputStream = System.in;
            int length = 0;
            char c;
            while ('\n' !=(c=(char) inputStream.read())){
                length++;
                if (' '== c){
                    length=0;
                }
            }
            System.out.println(length);

        }

}
