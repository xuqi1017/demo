package mutilThread;

public class LoopPrintABC {

    private volatile int value = 1;

    void printA() {
        synchronized (this) {
            while (value != 1) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName() + ": A");
            value = 2;
            notifyAll();
        }
    }

    void printB() {
        synchronized (this) {
            while (value != 2) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName() + ": B");
            value = 3;
            notifyAll();
        }
    }

    void printC() {
        synchronized (this) {
            while (value != 3) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName() + ": C");
            value = 1;
            notifyAll();
        }
    }
    public static void main(String[] args) {

        int loopNum = 5;

        LoopPrintABC loopPrintABC = new LoopPrintABC();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < loopNum; i++) {
                    loopPrintABC.printA();
                }
            }
        }, "线程一").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < loopNum; i++) {
                    loopPrintABC.printB();
                }
            }
        }, "线程二").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < loopNum; i++) {
                    loopPrintABC.printC();
                }
            }
        }, "线程三").start();

    }

}

