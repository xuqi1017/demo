package leetCode;

/**
 * 输入: 1->2->3->3->4->4->5
 * 输出: 1->2->5
 */
public class DeleteDuplicates {

    public static void main(String[] args) {
        LinkedNode head = LinkedNode.initLink(new int[]{1, 2, 3, 3, 4, 4, 5, 6, 9});
        LinkedNode.print(head);
        LinkedNode linkedNode = deleteDuplicates(head);
        //LinkedNode linkedNode = reverseRecursion(head);
        //LinkedNode linkedNode = exchange(head, 2, 5);
        //LinkedNode linkedNode = insertSort(head);
        //LinkedNode linkedNode = mergeSort(head);
        LinkedNode.print(linkedNode);
    }

    public static LinkedNode deleteDuplicates(LinkedNode head) {
        if (head == null) {
            return head;
        }

        LinkedNode res = new LinkedNode(0);
        res.next = head;

        LinkedNode p1 = res;
        LinkedNode p2 = head;
        while (p1.next != null) {

            if (p2.val == p2.next.val) {
                p1.next = p2.next;
            }
        }
        return res.next;
    }
}
