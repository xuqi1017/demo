package leetCode.dp;

/**
 * 接雨水
 *
 * @Date: 2023/12/8
 * @Author: xuqi
 * @Description:
 */
public class Rain {

    public static void main(String[] args) {
        int[] p = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
        System.out.println(Solution2(p));
    }

    /**
     * 暴力
     */
    public static int Solution1(int[] arr) {
        if (arr == null || arr.length == 1) {
            return 0;
        }
        int result = 0;
        for (int i = 0; i < arr.length; i++) {
            int left = 0;
            int right = 0;
            for (int j = 0; j < i; j++) {
                left = Math.max(arr[j], left);
            }
            for (int j = i + 1; j < arr.length; j++) {
                right = Math.max(arr[j], right);
            }
            if (Math.min(left, right) - arr[i] > 0) {
                result += Math.min(left, right) - arr[i];
            }
        }
        return result;
    }


    public static int Solution2(int[] arr) {
        if (arr == null || arr.length == 1) {
            return 0;
        }
        int result = 0;
        int[] left = new int[arr.length];
        int[] right = new int[arr.length];
        left[0] = arr[0];
        right[arr.length - 1] = arr[arr.length - 1];
        for (int i = 1; i < arr.length; i++) {
            left[i] = Math.max(left[i - 1], arr[i]);
        }
        for (int i = arr.length - 2; i >= 0; i--) {
            right[i] = Math.max(right[i + 1], arr[i]);
        }
        for (int i = 0; i < arr.length; i++) {
            result += Math.min(left[i], right[i]) - arr[i];
        }
        return result;
    }
}
