package leetCode.dp;

/**
 * 一条包含字母 A-Z 的消息通过以下映射进行了 编码 ：
 * <p>
 * 'A' -> 1
 * 'B' -> 2
 * ...
 * 'Z' -> 26
 * 要 解码 已编码的消息，所有数字必须基于上述映射的方法，反向映射回字母（可能有多种方法）。例如，"11106" 可以映射为：
 * <p>
 * "AAJF" ，将消息分组为 (1 1 10 6)
 * "KJF" ，将消息分组为 (11 10 6)
 * 注意，消息不能分组为  (1 11 06) ，因为 "06" 不能映射为 "F" ，这是由于 "6" 和 "06" 在映射中并不等价。
 * <p>
 * 给你一个只含数字的 非空 字符串 s ，请计算并返回 解码 方法的 总数 。
 * <p>
 * 题目数据保证答案肯定是一个 32 位 的整数。
 * <p>
 *  
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "12"
 * 输出：2
 * 解释：它可以解码为 "AB"（1 2）或者 "L"（12）。
 * 示例 2：
 * <p>
 * 输入：s = "226"
 * 输出：3
 * 解释：它可以解码为 "BZ" (2 26), "VF" (22 6), 或者 "BBF" (2 2 6) 。
 * 示例 3：
 * <p>
 * 输入：s = "0"
 * 输出：0
 * 解释：没有字符映射到以 0 开头的数字。
 * 含有 0 的有效映射是 'J' -> "10" 和 'T'-> "20" 。
 * 由于没有字符，因此没有有效的方法对此进行解码，因为所有数字都需要映射。
 * 示例 4：
 * <p>
 * 输入：s = "06"
 * 输出：0
 * 解释："06" 不能映射到 "F" ，因为字符串含有前导 0（"6" 和 "06" 在映射中并不等价）。
 *  
 * <p>
 * 提示：
 * <p>
 * 1 <= s.length <= 100
 * s 只包含数字，并且可能包含前导零。
 *
 * @Date: 2022/1/5
 * @Author: xuqi
 * @Description:
 */
public class DecodeWays {
    public static int numDecodings(String s) {
        int length = s.length();
        if (s.charAt(0) == '0'){
            return 0;
        }
        int preNum = 1;
        int curNum = length < 2 ? 1 : decode(s.charAt(0), s.charAt(1));
        for (int i = 2; i < length; i++) {
            char preChar = s.charAt(i - 1);
            char curChar = s.charAt(i);
            int oldCurNum = curNum;
            if (curChar == '0'){
                if (preChar == '0'){
                    return 0;
                } else {
                    if (preChar > '2'){
                        return 0;
                    }
                    curNum = preNum;
                    preNum = oldCurNum;
                }
            } else {
                if (preChar == '0'){
                    preNum = oldCurNum;
                } else {
                    curNum = decode(preChar, curChar) > 1 ? preNum + curNum : curNum;
                    preNum = oldCurNum;
                }
            }
        }
        return curNum;

    }
    private static int decode(char preChar, char curChar) {
        if (preChar > '2' && curChar == '0'){
            return 0;
        } else if (curChar == '0'){
            return 1;
        }
        if (preChar > '2'){
            return 1;
        } else if (preChar == '2' && curChar > '6'){
            return 1;
        }
        return 2;
    }


    public static void main(String[] args) {
        System.out.println(numDecodings("2301"));
    }
}
