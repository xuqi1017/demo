package leetCode.dp;

/**
 * 给定一个整数数组，其中第 i 个元素代表了第 i 天的股票价格 。​
 * <p>
 * 设计一个算法计算出最大利润。在满足以下约束条件下，你可以尽可能地完成更多的交易（多次买卖一支股票）:
 * <p>
 * 你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
 * 卖出股票后，你无法在第二天买入股票 (即冷冻期为 1 天)。
 * <p>
 * 示例:
 * <p>
 * 输入: [1,2,3,0,2]
 * 输出: 3
 * 解释: 对应的交易状态为: [买入, 卖出, 冷冻期, 买入, 卖出]
 *
 * @Date: 2022/1/6
 * @Author: xuqi
 * @Description:
 */
public class StockColldown {

    public static int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int buy = -prices[0], sell = 0, sellCool = 0;
        int tmp ;
        for (int i = 1; i < prices.length; i++) {
            tmp = sellCool;
            sellCool = buy + prices[i];
            buy = Math.max(buy, sell - prices[i]);
            sell = Math.max(sell, tmp);
        }
        return Math.max(sellCool, sell);
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 3, 0, 2};
        System.out.println(maxProfit(a));
    }
}
