package leetCode.dp;

/**
 * 给你一个整数数组 nums ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。
 * <p>
 *  
 * <p>
 * 示例 1:
 * <p>
 * 输入: [2,3,-2,4]
 * 输出: 6
 * 解释: 子数组 [2,3] 有最大乘积 6。
 * 示例 2:
 * <p>
 * 输入: [-2,0,-1]
 * 输出: 0
 * 解释: 结果不能为 2, 因为 [-2,-1] 不是子数组。
 * <p>
 * 链接：https://leetcode-cn.com/problems/maximum-product-subarray
 *
 * @Date: 2022/1/7
 * @Author: xuqi
 * @Description:
 */
public class MaxProductSubarray {
    public static int maxProduct(int[] nums) {
        int n = nums.length;
        if (n == 0) {
            return 0;
        }
        int max = nums[0];
        int min = nums[0];
        int ans = nums[0];
        for (int i = 1; i < n; i++) {
            int mx = max, mn = min;
            max = Math.max(mx * nums[i ], Math.max(mn * nums[i], nums[i]));
            min = Math.min(mx * nums[i ], Math.min(mn * nums[i ], nums[i]));
            ans = Math.max(ans, max);
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] a = {-2, 0, -1};
        System.out.println(maxProduct(a));
    }
}
