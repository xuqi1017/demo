package leetCode.dp;

/**
 * 给定一个整数数组 prices ，它的第 i 个元素 prices[i] 是一支给定的股票在第 i 天的价格。
 * <p>
 * 设计一个算法来计算你所能获取的最大利润。你最多可以完成 k 笔交易。
 * <p>
 * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
 * <p>
 *  
 * <p>
 * 示例 1：
 * <p>
 * 输入：k = 2, prices = [2,4,1]
 * 输出：2
 * 解释：在第 1 天 (股票价格 = 2) 的时候买入，在第 2 天 (股票价格 = 4) 的时候卖出，这笔交易所能获得利润 = 4-2 = 2 。
 * 示例 2：
 * <p>
 * 输入：k = 2, prices = [3,2,6,5,0,3]
 * 输出：7
 * 解释：在第 2 天 (股票价格 = 2) 的时候买入，在第 3 天 (股票价格 = 6) 的时候卖出, 这笔交易所能获得利润 = 6-2 = 4 。
 * 随后，在第 5 天 (股票价格 = 0) 的时候买入，在第 6 天 (股票价格 = 3) 的时候卖出, 这笔交易所能获得利润 = 3-0 = 3 。
 *  
 *
 * @Date: 2022/1/6
 * @Author: xuqi
 * @Description:
 */
public class StockIV {
    public static int maxProfit(int k, int[] prices) {
        if (prices == null || prices.length == 0 || k == 0) {
            return 0;
        }
        int[] b = new int[k];
        int[] s = new int[k];
        for (int i = 0; i < k; i++) {
            b[i] = -prices[0];
            s[i] = 0;
        }
        for (int i = 1; i < prices.length; i++) {
            b[0] = Math.max(b[0], -prices[i]);
            s[0] = Math.max(s[0], b[0] + prices[i]);
            for (int j = 1; j < k; j++) {
                b[j] = Math.max(b[j], s[j - 1] - prices[i]);
                s[j] = Math.max(s[j], b[j] + prices[i]);
            }
        }
        return s[k - 1];
    }

    public static void main(String[] args) {
        int[] a = {3, 2, 6, 5, 0, 3};
        System.out.println(maxProfit(2, a));
    }
}
