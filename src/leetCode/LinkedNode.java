package leetCode;

public class LinkedNode {
    public int val;
    public LinkedNode next;

    public LinkedNode(int x) {
        val = x;
    }

    public static LinkedNode initLink(int[] vals) {
        if (vals == null || vals.length == 0) {
            return null;
        }
        LinkedNode pre = new LinkedNode(0);
        LinkedNode cur = pre;
        for (int val : vals) {
            LinkedNode newNode = new LinkedNode(val);
            cur.next = newNode;
            cur = newNode;
        }
        return pre.next;
    }

    public static void print(LinkedNode head) {
        while (head != null) {
            System.out.print(head.val);
            System.out.print("  ");
            head = head.next;
        }
        System.out.println();
    }

    public static LinkedNode findTail(LinkedNode head) {
        if (head==null){
            return head;
        }
        while (head.next != null) {
            head = head.next;
        }
        return head;

    }
}
