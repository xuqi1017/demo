package leetCode.greedy;

/**
 * n*n的棋盘上，要求任何两个皇后不同行不同列也不在一条斜线上
 *
 * @Date: 2023/12/4
 * @Author: xuqi
 * @Description:
 */
public class NQueen {
    // 暴力
    public static int nQueen1(int n) {
        if (n < 1) {
            return 0;
        }
        int[] record = new int[n];
        return process1(record, 0, n);
    }

    private static int process1(int[] record, int i, int n) {
        if (i == n) {
            return 1;
        }
        int res = 0;
        for (int j = 0; j < n; j++) {
            if (isValid(record, i, j)) {
                record[i] = j;
                res += process1(record, i + 1, n);
            }
        }
        return res;
    }

    private static boolean isValid(int[] record, int i, int j) {
        for (int k = 0; k < i; k++) {
            if (record[k] == j || //共列
                    Math.abs(record[k] - j) == Math.abs(k - i)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(nQueen1(15));
    }

}
