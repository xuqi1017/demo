package leetCode.greedy;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 输入项目的成本数组，利润数组，最多做几个项目，初始本金，求最多赚多少钱
 *
 * @Date: 2023/12/4
 * @Author: xuqi
 * @Description:
 */
public class MaxProfit {
    static class Node {
        int cost;
        int profit;

        Node(int cost, int profit) {
            this.profit = profit;
            this.cost = cost;
        }
    }

    class MaxCompare implements Comparator<Node> {

        @Override
        public int compare(Node o1, Node o2) {
            return o2.profit - o1.profit;
        }
    }

    class MinCompare implements Comparator<Node> {

        @Override
        public int compare(Node o1, Node o2) {
            return o1.cost - o2.cost;
        }
    }

    public static int maxProfit(int[] cost, int[] profit, int k, int money) {
        // 贪心策略，每次找到能做的最赚的，循环k次
        // 用两个堆来优化k次遍历,先选出花费最小的堆，然后从这个堆中依次出可以做的项目，按利润最大入另一个堆
        PriorityQueue<Node> maxProfit = new PriorityQueue<>();
        PriorityQueue<Node> minCost = new PriorityQueue<>();
        for (int i = 0; i < cost.length; i++) {
            minCost.add(new Node(cost[i], profit[i]));
        }
        for (int i = 0; i < k; i++) {
            while (!minCost.isEmpty() && money >= minCost.peek().cost) {
                maxProfit.add(minCost.poll());
            }
            if (!maxProfit.isEmpty()) {
                money += maxProfit.poll().profit;
            } else {
                return money;
            }
        }
        return money;
    }
}
