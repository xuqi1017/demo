package leetCode.greedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 题目描述：
 * <p>
 * 字符串 S 由小写字母组成。我们要把这个字符串划分为尽可能多的片段，同一个字母只会出现在其中的一个片段。返回一个表示每个字符串片段的长度的列表。
 * <p>
 * 示例 1:
 * <p>
 * 输入: S = "ababcbacadefegdehijhklij"
 * 输出: [9,7,8]
 * 解释:
 * 划分结果为 "ababcbaca", "defegde", "hijhklij"。
 * 每个字母最多出现在一个片段中。
 * 像 "ababcbacadefegde", "hijhklij" 的划分是错误的，因为划分的片段数较少。
 */
public class PartitionString {

    public static List<Integer> partitionString(String s) {
        ArrayList<Integer> result = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return result;
        }
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.toCharArray().length; i++) {
            map.put(s.charAt(i), i);
        }
        int start = 0;
        int end = 0;
        for (int i = 0; i < s.toCharArray().length; i++) {
            end = Math.max(end, map.get(s.charAt(i)));
            if (end == i) {
                result.add(end - start + 1);
                start = end + 1;
            }
        }
        return result;
    }

}
