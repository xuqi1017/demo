package leetCode;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @Date: 2023/12/15
 * @Author: xuqi
 * @Description:
 */
public class ReverseWords151 {
    public static String reverseWords(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        int n = s.length();
        StringBuilder ans = new StringBuilder();
        int left = n - 1, right = n ;
        for (; left >= 0; left--) {
            if (s.charAt(left) == ' ') {
                ans.append(s, left + 1, right);
                ans.append(' ');
                right = left;
            }
        }
        ans.append(s, left + 1, right);
        return ans.toString();
    }

    public static void main(String[] args) {
        System.out.println(reverseWords("aa afs saf as fas fa sf as f asf asfd"));
    }
}
