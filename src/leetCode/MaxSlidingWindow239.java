package leetCode;

import java.util.PriorityQueue;

public class MaxSlidingWindow239 {
    public static void main(String[] args) {
        int[] ints = {1, 2, 3, 13, 13, 4, 13};
        int[] ints1 = maxSlidingWindow(ints, 3);
        System.out.println();
    }
    public static int[] maxSlidingWindow(int[] arr, int k) {
        if (arr == null) {
            return null;
        }
        int[] res = new int[arr.length - k+1];
        PriorityQueue<Integer> queue = new PriorityQueue<>(((o1, o2) -> (o2 - o1)));
        for (int i = 0; i < k; i++) {
            queue.add(arr[i]);
        }
        int j=0;
        res[j++]=queue.peek();
        for (int i = k; i < arr.length ; i++) {
            queue.remove(arr[i-k]);
            queue.add(arr[i]);
            res[j++]=queue.peek();
        }
        return res;

    }
}
