package leetCode;

/**
 * @Date: 2023/12/14
 * @Author: xuqi
 * @Description:
 */
public class IntToRoman12 {


    public static String intToRoman(int num) {
        int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] symbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < values.length; i++) {
            while (num>=values[i]){
                sb.append(symbols[i]);
                num-=values[i];
            }
            if (num==0){
                break;
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(intToRoman(124897));
    }
}
