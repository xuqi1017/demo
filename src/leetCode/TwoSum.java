package leetCode;
/*
167
给定一个已按照升序排列 的有序数组，找到两个数使得它们相加之和等于目标数。

函数应该返回这两个下标值 index1 和 index2，其中 index1 必须小于 index2。*/

import java.util.Arrays;
import java.util.HashMap;

public class TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        int[] ans = new int[2];
        if(nums==null)
        {
            return null;
        }
        int i = 0;
        int j = nums.length-1;
        while (true){
            if (target-nums[i]-nums[j]>0){
                i++;
            }else if(target-nums[i]-nums[j]<0){
                j--;
            }else {
                return new int[] {i+1,j+1};
            }
        }

        //return ans;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(twoSum(new int[]{2, 7, 11, 15}, 9)));
    }
    public int[] twoSum1(int[] nums, int target) {
        int[] ans = new int[2];
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i <nums.length ; i++) {
            if (map.containsKey(nums[i])){
                return new int[] {map.get(nums[i]),i};
            }else {
                map.put(target-nums[i],i);
            }
        }

        return ans;
    }
}
