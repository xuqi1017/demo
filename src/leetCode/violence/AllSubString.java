package leetCode.violence;

import java.util.LinkedHashMap;

public class AllSubString {

    public static void main(String[] args) {
        allSubString("abkfghgfc");
    }

    public static void allSubString(String s) {
        int[] has = new int[s.length()];
        subString(s.toCharArray(), has, -1);
    }

    private static void subString(char[] toCharArray, int[] has, int curBit) {
        if (curBit == has.length-1) {
            printSubString(toCharArray, has);
            return;
        }
        has[curBit+1] = 1;
        subString(toCharArray, has, curBit+1);
        has[curBit+1] = 0;
        subString(toCharArray, has, curBit+1);
    }

    private static void printSubString(char[] toCharArray, int[] has) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < toCharArray.length; i++) {
            if (has[i] == 1) {
                sb.append(toCharArray[i]);
            }
        }
        System.out.println(sb.toString());
    }
}
