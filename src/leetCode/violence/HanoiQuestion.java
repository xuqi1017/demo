package leetCode.violence;

public class HanoiQuestion {

    public static void func(int i ,String start,String end ,String other){
        if (i==1){
            System.out.println("move "+i+" from "+start+" to "+end);
        }else {
            func(i-1,start,other,end);
            System.out.println("move "+i+" from "+start+" to "+end);
            func(i-1,other,end,start);
        }
    }

    public static void main(String[] args) {
        func(64,"左","右","中");
    }

}
