package leetCode;

import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Random;

public class TopK {

    public static void main(String[] args) {
        int[] ints = {1, 2, 3, 13, 13, 4, 13};
        System.out.println(heap(ints, 3));
        System.out.println(quickSort(ints, 0,ints.length-1,5000));

    }

    /**
     * 不是最优解 是nlogk,但适合大数据量
     *
     * @param arr
     * @param k
     * @return
     */
    public static int heap(int[] arr, int k) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        for (int i : arr) {
            if (queue.size() < k) {
                queue.add(i);
            } else {
                if (queue.peek() < i) {
                    queue.poll();
                    queue.add(i);
                }
            }
        }
        return queue.poll();
    }


    /**
     * 使用快排中的partition
     * 用主方法分析是 复杂度是 n
     *
     * @param arr
     * @param k
     * @return
     */
    public static int quickSort(int[] arr, int left, int right, int k) {
        int p = partition(arr, left, right);
        if (p == k-1) {
            return arr[p];
        }
        if (p > k-1) {
            return quickSort(arr, left, p - 1, k);
        }
        return quickSort(arr, p + 1, right, k);
    }

    public static int partition(int[] arr, int left, int right) {
        int x = arr[right];
        int smallIndex = left;
        for (int i = left; i < right; i++) {
            if (arr[i] >= x) {
                swap(arr, i, smallIndex);
                smallIndex++;
            }
        }
        swap(arr, smallIndex, right);
        return smallIndex;
    }

    private static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

}
