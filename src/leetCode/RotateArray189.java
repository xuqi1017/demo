package leetCode;

import java.util.Arrays;

/**
 * @Date: 2023/12/14
 * @Author: xuqi
 * @Description:
 */
public class RotateArray189 {
    public static void rotate(int[] nums, int k) {
        if (nums == null || nums.length < 2 || k == 0) {
            return;
        }
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k-1);
        reverse(nums, k, nums.length - 1);

    }

    private static void reverse(int[] nums, int i, int j) {
        while (j > i) {
            swap(nums,i++,j--);
        }
    }

    private static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] =  nums[j];
        nums[j] = temp;
    }


    public static void main(String[] args) {
        int[] nums = {-1, -100, 3, 99};
        rotate(nums, 2);
        System.out.println(Arrays.toString(nums));

    }
}
