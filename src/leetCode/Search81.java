package leetCode;
/*输入: nums = [2,5,6,0,0,1,2], target = 0
        输出: true*/
public class Search81 {
    public boolean search(int[] nums, int target) {
        //
        if (nums.length==0)return false;
        int start = 0;
        int mid = 0;
        int end = nums.length-1;
        while (start<=end){
            mid = start+(end-start)/2;
            if (target==nums[mid])return true;
            if (nums[start]==nums[mid]){
                start++;
                continue;
            }
            if (nums[start]<nums[mid]){
                //前有序
                if (target>=nums[start]&&target<nums[mid])
                    end = mid-1;
                else start=mid+1;
            }else {
                //后有序
                if (target>nums[mid]&&target<=nums[end])
                    start=mid+1;
                else end = mid-1;
            }
        }
        return false;
    }
}
