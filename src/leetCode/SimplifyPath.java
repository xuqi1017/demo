package leetCode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/*一个点（.）表示当前目录本身；此外，两个点 （..） 表示将目录切换到上一级（指向父目录）*/
public class SimplifyPath {
    public static String simplifyPath(String path) {
        Stack<String> stack = new Stack<>();
        for (String s : path.split("/")) {
            if ("..".equals(s)) {
                if (!stack.empty()) {
                    stack.pop();
                }
            }else if ( !s.isEmpty() && !".".equals(s) ) {
                stack.push(s);
            }
        }
        StringBuffer sb = new StringBuffer();
        for (String s : stack) {
            sb.append("/");
            sb.append(s);
        }
        String res = sb.toString();
        return res.isEmpty() ? "/" : res;
    }

    public static void main(String[] args) {
        System.out.println(simplifyPath("/a/../../b/../c//.//"));
    }

}
