package leetCode;
//单词搜索 79
/*board =
        [
        ['A','B','C','E'],
        ['S','F','C','S'],
        ['A','D','E','E']
        ]

        给定 word = "ABCCED", 返回 true
        给定 word = "SEE", 返回 true
        给定 word = "ABCB", 返回 false
 */
public class Exist {
        // DFS, also is backtrack
        public boolean exist(char[][] board, String word) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[0].length; j++) {
                    if (exist(board, i, j, word, 0)) {
                        return true;
                    }
                }
            }

            return false;
        }

        public boolean exist(char[][] board, int i, int j, String word, int start) {
            if (start >= word.length())
                return true;

            if (i < 0 || i >= board.length || j < 0 || j >= board[0].length)
                return false;

            if (board[i][j] == word.charAt(start)) {
                char c = board[i][j];
                start++;

                // select current char and change to '#' which indicated it has been visited
                board[i][j] = '#';

                // backtrack, check positions on upper, lower lines, left and right positions
                boolean res = exist(board, i - 1, j, word, start) || exist(board, i + 1, j, word, start)
                        || exist(board, i, j - 1, word, start) || exist(board, i, j + 1, word, start);

                // unselect the current char
                board[i][j] = c;

                return res;
            }

            return false;
        }


    public static void main(String[] args) {
        char[][] board = {
                {'A','B','C','E'},
                {'S','F','E','S'},
                {'A','D','E','E'}
                };
        String word = "ABCESEEEFS";
        //System.out.println(exist(board,word));
    }

}