package leetCode;

/**
 * @Date: 2023/12/15
 * @Author: xuqi
 * @Description:
 */
public class LinkedSum {
    public LinkedNode addTwoNumbers(LinkedNode l1, LinkedNode l2) {
        LinkedNode newHead = new LinkedNode(0);
        LinkedNode nextNode = newHead;
        boolean flag = false;
        while (l1 != null || l2 != null) {
            int curVal = flag ? 1 : 0;
            if (l1 != null) {
                curVal += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                curVal += l2.val;
                l2 = l2.next;
            }
            if (curVal > 9) {
                flag = true;
                curVal -= 10;
            }else {
                flag = false;
            }
            nextNode.next = new LinkedNode(curVal);
            nextNode = nextNode.next;
        }
        if (flag){
            nextNode.next = new LinkedNode(1);
        }
        return newHead.next;
    }
}
