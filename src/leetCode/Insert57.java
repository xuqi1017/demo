package leetCode;


import java.util.LinkedList;

/**
 * 输入：intervals = [[1,2],[3,5],[8,10],[12,16]], newInterval = [4,8]
 *         输出：[[1,2],[3,10],[12,16]]
 *         解释：这是因为新的区间 [4,8] 与 [3,5],[6,7],[8,10] 重叠。
 */

public class Insert57 {
    public int[][] insert(int[][] intervals, int[] newInterval) {
        int inserPos = 0;
        LinkedList<int[]> output = new LinkedList<>();
        for (int[] interval : intervals) {
           if (interval[1]<newInterval[0]){
               output.add(interval);
               inserPos++;
           }
           else  if (interval[0]>newInterval[1]) {
               output.add(interval);
           } else {
               newInterval[0] = Math.min(newInterval[0],interval[0]);
               newInterval[1] = Math.max(newInterval[1],interval[1]);
           }
        }
        output.add(inserPos,newInterval);
        return  output.toArray(new int[output.size()][2]);

    }
}
