package leetCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Date: 2023/11/29
 * @Author: xuqi
 * @Description:
 */
public class StringSplit {

    public static void main(String[] args) {
        List<String> split = split("asf,afaw,asf,aa,sf,aas,f,asf,a,as,afa,s", ",a");
        System.out.println();
    }

    public static List<String> split(String s, String regex) {
        List<String> result = new ArrayList<>();
        if (s == null || regex == null) {
            return result;
        }
        int i = 0, j = 0;
        while (j < s.length()) {
            if (check(s, j, regex)) {
                result.add(s.substring(i, j));
                j = j + regex.length();
                i = j;
            } else {
                j++;
            }
        }
        if (i != j) {
            result.add(s.substring(i, j));
        }
        return result;
    }

    private static boolean check(String s, int j, String regex) {
        if (j + regex.length() >= s.length()) {
            return false;
        }
        return s.substring(j, j + regex.length()).equals(regex);
    }
}
