package leetCode;

import java.util.concurrent.ConcurrentHashMap;

public class CountAndSay {
   
/*1.     1
        2.     11
        3.     21
        4.     1211
        5.     111221*/
    public static String countAndSay(int n){
        String ans ="1";
        for (int i = 1; i <n ; i++) {
            ans =count1(ans );
        }
        return  ans ;
    }

    private static String count1(String ans) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 1;//当前数
        int pre = 0;
        while (i<ans.length()){
            if (ans.charAt(i)!=ans.charAt(i-1)){
                stringBuffer.append(i-pre);
                stringBuffer.append(ans.charAt(pre));
                pre=i;
            }
            i++;
        }
        stringBuffer.append(i-pre);
        stringBuffer.append(ans.charAt(pre));
        return stringBuffer.toString();
    }

    private static String count(String last) {
        String ans = "";
        int count = 0;
        char tmpChar = '0';
        char[] chars=last.toCharArray();
        for (char c : chars) {
            if (count>0){
                if(c==tmpChar){
                    count++;
                }else {
                    ans=ans+count+tmpChar;
                    tmpChar=c;
                    count=1;
                }
            }else {
                count++;
                tmpChar = c;
            }
        }
        if (count>0){
            ans=ans+count+tmpChar;
        }
        System.out.println(ans);
        return ans ;
    }

    public static void main(String[] args) {
        System.out.println(countAndSay(7));

    }
}
