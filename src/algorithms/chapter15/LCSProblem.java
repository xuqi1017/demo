package algorithms.chapter15;

import java.util.Arrays;

/**
 * 最长公共子串
 *
 * @Date: 2021/12/31
 * @Author: xuqi
 * @Description:
 */
public class LCSProblem {

    public static void LCS(char[] a1, char[] a2) {
        int[][] r = new int[a1.length + 1][a2.length + 1];
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a1[i] == a2[j]) {
                    r[i + 1][j + 1] = r[i][j] + 1;
                } else if (r[i + 1][j] > r[i][j + 1]) {
                    r[i + 1][j + 1] = r[i + 1][j];
                } else {
                    r[i + 1][j + 1] = r[i][j + 1];

                }
            }
        }
        for (int[] row : r) {
            System.out.println(Arrays.toString(row));
        }
    }

    public static void main(String[] args) {
        String a1 = "ABCBDAB";
        String a2 = "BDCABA";
        LCS(a1.toCharArray(), a2.toCharArray());
    }
}
