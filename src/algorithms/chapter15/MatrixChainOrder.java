package algorithms.chapter15;

import java.util.Arrays;

/**
 * @Date: 2021/12/30
 * @Author: xuqi
 * @Description:
 */
public class MatrixChainOrder {
    public static void matrixChainOrder(int[] p) {
        //结果
        int[][] m = new int[p.length - 1][p.length - 1];
        //分割点
        int[][] s = new int[p.length - 1][p.length - 1];
        for (int i = 1; i < m.length; i++) {
            for (int j = 0; j < m.length - i; j++) {
                int q = Integer.MAX_VALUE;
                for (int k = j; k < j + i; k++) {
                    if (q > m[j][k] + m[k + 1][j + i] + p[j] * p[k + 1] * p[j + i + 1]) {
                        q = m[j][k] + m[k + 1][j + i] + p[j] * p[k + 1] * p[j + i + 1];
                    }
                }
                m[j][j + i] = q;
            }
        }

        for (int[] row : m) {
            System.out.println(Arrays.toString(row));
        }
    }

    public static void main(String[] args) {
        //a.length = 11,10个矩阵
        int[] a = {30, 35, 15, 5, 10, 20, 25};
        matrixChainOrder(a);
    }
}
