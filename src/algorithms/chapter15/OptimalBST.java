package algorithms.chapter15;

import java.util.Arrays;

/**
 * @Date: 2021/12/31
 * @Author: xuqi
 * @Description:
 */
public class OptimalBST {
    public static void optimalBST(double[] p, double[] q) {
        int length = p.length;
        //结果-最小期望
        // 期望 = 深度*概率
        double[][] e = new double[length + 1][length + 1];
        //结果-根节点
        int[][] root = new int[length + 2][length + 1];
        // 存储总概率
        double[][] w = new double[length + 2][length + 1];
        //长度
        for (int i = 1; i <= length+1; i++) {
            w[i][i - 1] = q[i - 1];
            w[i][i - 1] = q[i - 1];
        }
        for (int l = 1; l <= length ; l++) {
            //起始关键字
            for (int i = 1; i <= length-l; i++) {
                //不论以哪个节点为根节点，总概率不变
                e[i][i + l] = Double.MAX_VALUE;
                w[i][i + l] = w[i][i + l - 1] + p[i + l - 1] + q[i + l ];
                for (int k = i; k <= l; k++) {
                    //分割点或根节点
                    //结果为中间点期望加左右最小期望+伪关键字期望
                    if (e[i][i + l] < e[i][i + k - 1] + e[i + k + 1][i  +l]) {
                        e[i][i + l] = e[i][i + k - 1] + e[i + k + 1][i + l];
                        root[i][i + l] = k;
                    }
                }
                e[i][i + l] += w[i][i + l];

            }
        }
        for (double[] row : w) {
            System.out.println(Arrays.toString(row));
        }
        System.out.println("结果为" + w[1][length]);
        // 先序遍历根节点
        preOrder(root, 1, length );
    }

    private static void preOrder(int[][] root, int i, int r) {
        if (r < i||root[i][r]==0) {
            return;
        }
        System.out.print(root[i][r]);
        preOrder(root, i, root[i][r] - 1);
        preOrder(root, root[i][r] + 1, r);

    }

    public static void main(String[] args) {
        //a.length = 11,10个矩阵
        double[] p = {0.15, 0.1, 0.05, 0.1, 0.2};
        double[] q = {0.05, 0.1, 0.05, 0.05, 0.05, 0.1};

        optimalBST(p, q);
    }
}
