package algorithms.chapter15;

/**
 * 动态规划切割钢条，
 * 每次切割都有一个常数消耗c
 *
 * @Date: 2021/12/30
 * @Author: xuqi
 * @Description:
 */
public class CutRod {
    public static void cutRod(int[] profit, int cost, int length) {
        if (length < 1) {
            System.out.println(0);
            return;
        }
        int[] r = new int[length+1];
        int[] s = new int[length+1];
        for (int i = 1; i <= length; i++) {
            int q = -1;
            for (int j = 0; j < i; j++) {
                if (q < profit[i - j-1] + r[j] - cost) {
                    q =  profit[i - j-1] + r[j] - cost;
                    s[i] = j;
                }
                r[i] = q;
            }
        }
        System.out.println(r[length]);

        System.out.println(s[length]);
    }

    public static void main(String[] args) {
        int[] a = {1, 5, 8, 9, 10, 17, 17, 20, 24, 30};

        cutRod(a,2,9);
    }
}
