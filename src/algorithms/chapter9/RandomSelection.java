package algorithms.chapter9;

/**
 * 找出第i小的数
 *
 * @Date: 2021/12/24
 * @Author: xuqi
 * @Description:
 */
public class RandomSelection {
    public static int randomSelection(int[] a, int left, int right, int rank) {
        if (left >= right) {
            return a[left];
        }
        int p = randomPartition(a, left, right);
        int k = p - left;
        if (rank == k) {
            return a[p];
        }
        if (rank < k) {
            return randomSelection(a, left, p - 1, rank);
        } else {
            return randomSelection(a, p + 1, right, rank - k);
        }
    }

    private static int randomPartition(int[] a, int left, int right) {
        int x = a[right];
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (a[j] <= x) {
                i++;
                exchange(a, i, j);
            }
        }
        exchange(a, i + 1, right);
        return i + 1;
    }

    private static void exchange(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    public static void main(String[] args) {
        int[] a = {1, 9, 5, 23, 6, 8, 346, 6, 23, 23, 745, 123, 124, 4, 6, 4667, 5};
        int i1 = randomSelection(a, 0, a.length - 1, 2);
        System.out.println(i1);
    }
}
