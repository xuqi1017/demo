package algorithms.chapter8;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @Date: 2023/11/22
 * @Author: xuqi
 * @Description:
 */
public class RadixSort {

    public static void radixSort(int[] arr) {
        int digit = maxDigit(arr);
        LinkedList<Queue<Integer>> bucket = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            bucket.add(new LinkedList<>());
        }
        int radix = 1;
        for (int i = 0; i < digit; i++) {
            radix *= 10;
            for (int num : arr) {
                bucket.get(num % radix * 10 / radix).add(num);
            }
            int j = 0;
            for (Queue<Integer> queue : bucket) {
                while (!queue.isEmpty()) {
                    arr[j++] = queue.poll();
                }
            }
        }
    }

    public static void radixSortOptimize(int[] arr) {
        int digit = maxDigit(arr);
        int radix = 1;
        int[] help = new int[arr.length];
        for (int i = 0; i < digit; i++) {
            radix *= 10;
            int[] counts = new int[10];
            for (int num : arr) {
                int bitNum = num % radix * 10 / radix;
                counts[bitNum]++;
            }
            for (int j = 1; j < counts.length; j++) {
                counts[j] = counts[j - 1] + counts[j];
            }
            for (int k = arr.length - 1; k >= 0; k--) {
                int bitNum = arr[k] % radix * 10 / radix;
                counts[bitNum]--;
                help[counts[bitNum]] = arr[k];
            }
            for (int x = 0; x < arr.length; x++) {
                arr[x] = help[x];
            }
        }

    }

    public static void main(String[] args) {
        int[] a = {1, 2, 5, 23, 6, 8, 346, 6, 23, 23, 745, 123, 124, 4, 6, 4667, 5};
        radixSortOptimize(a);
        for (int i : a) {
            System.out.println(i);
        }
    }


    private static int maxDigit(int[] arr) {
        int res = 0;
        int max = Integer.MIN_VALUE;
        for (int i : arr) {
            max = Math.max(max, i);
        }
        while (max != 0) {
            res++;
            max /= 10;
        }
        return res;
    }
}
