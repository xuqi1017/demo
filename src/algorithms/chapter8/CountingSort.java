package algorithms.chapter8;

/**
 * @Date: 2021/12/24
 * @Author: xuqi
 * @Description:
 */
public class CountingSort {
    public static void countingSort(int[] a){
        int[] result = new int[a.length];
        int[] c = new int[11];
        for (int i : a) {
            c[i]++;
        }
        for (int i = 1; i < c.length; i++) {
            c[i] += c[i-1];
        }
        for (int i = a.length-1; i >=0; i--) {
            result[c[a[i]]-1] = a[i];
            c[a[i]]--;
        }
        for (int i : result) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 5, 3, 6, 8, 1, 6, 8, 0, 7, 6, 3, 4, 6, 7, 5};
        countingSort(a);
    }
}
