package algorithms.chapter2;

/**
 * @Date: 2023/11/17
 * @Author: xuqi
 * @Description:
 */
public class InsertSort {
    public static void insertSort(int[] num) {
        if (num == null || num.length < 2) {
            return;
        }
        for (int i = 1; i < num.length; i++) {
            for (int j = i-1; j > 0; j--) {
                if (num[j+1]<num[j]){
                    swap(num,j,j+1);
                }
            }
        }
    }

    private static void swap(int[] num, int j, int i) {
        num[i]= num[i]^num[j];
        num[j]= num[i]^num[j];
        num[i]= num[i]^num[j];
    }
}
