package algorithms.chapter2;

/**
 * 算法导论2.3.2
 * @Date: 2021/12/15
 * @Author: xuqi
 * @Description:
 */
public class MergeSort {
    private static void mergeSort(int[] a, int low, int high) {
        if (a == null || a.length < 1) {
            return;
        }
        if (low >= high) {
            return;
        }
        int mid = low + (high - low) / 2;
        mergeSort(a, low, mid);
        mergeSort(a, mid + 1, high);
        merge(a, low, mid, high);
    }

    /**
     * 用了一个哨兵，真实场景中不行
     * @param a
     * @param low
     * @param mid
     * @param high
     */
    private static void merge(int[] a, int low, int mid, int high) {
        int[] left = new int[mid - low + 2];
        int[] right = new int[high - mid + 1];
        for (int i = 0; i <= mid - low + 1; i++) {
            left[i] = a[low + i];
        }
        for (int i = 0; i <= high - mid - 1; i++) {
            right[i] = a[mid + i + 1];
        }
        left[left.length - 1] = Integer.MAX_VALUE;
        right[right.length - 1] = Integer.MAX_VALUE;

        for (int i = low, j = 0, k = 0; i <= high; ) {
            if (left[j] < right[k]) {
                a[i++] = left[j++];
            } else {
                a[i++] = right[k++];
            }
        }
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 5, 23, 6, 8, 346, 6, 23, 23, 745, 123, 124, 4, 6, 4667, 5};
        mergeSort(a, 0, a.length - 1);
        for (int i : a) {
            System.out.println(i);
        }
    }
}
