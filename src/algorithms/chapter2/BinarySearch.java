package algorithms.chapter2;

/**
 * 二分查找
 *
 * @Date: 2021/12/15
 * @Author: xuqi
 * @Description:
 */
public class BinarySearch {
    public static int binarySearch(int[] a, int v) {
        if (a == null || a.length < 1) {
            return -1;
        }
        int right = a.length;
        int left = 0;
        int mid = 0;
        while (mid < right) {
            mid = (right + left) / 2;
            if (v == a[mid]) {
                return mid;
            } else if (v > a[mid]) {
                left = mid;
            } else {
                right = mid;
            }
        }
        return -1;
    }
}
