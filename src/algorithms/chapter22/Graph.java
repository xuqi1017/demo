package algorithms.chapter22;


import leetCode.LinkedNode;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 图的表示方法
 * 点集+边集
 * 邻接表
 * 邻接矩阵
 *
 * @Date: 2023/11/24
 * @Author: xuqi
 * @Description:
 */
public class Graph {

    public HashMap<Integer, Node> nodes;
    public HashSet<Edge> edges;

    class Node {
        int v;
        int in; // 入度
        int out; // 出度
        ArrayList<Node> nexts;
        ArrayList<Edge> edges;

        Node(int v) {
            this.v = v;
            in = 0;
            out = 0;
            nexts = new ArrayList<>();
            edges = new ArrayList<>();
        }
    }

    class Edge implements Comparable<Edge> {
        Node from;
        Node to;
        int w; //权重

        Edge(int w, Node from, Node to) {
            this.w = w;
            this.from = from;
            this.to = to;
        }


        @Override
        public int compareTo(Edge o) {
            return this.w - o.w;
        }
    }

    public void bfs(Node node) {
        if (node == null) {
            return;
        }
        Queue<Node> queue = new LinkedList<>();
        // 一般笔面试，Node一般就是一个value，可以用数组来优化hashSet
        HashSet<Node> set = new HashSet<>();
        queue.offer(node);
        while (!queue.isEmpty()) {
            Node poll = queue.poll();
            System.out.println(poll.v);
            for (Node next : poll.nexts) {
                if (!set.contains(next)) {
                    queue.offer(next);
                    set.add(next);
                }
            }
        }
    }

    public void dfs(Node node) {
        if (node == null) {
            return;
        }
        Stack<Node> stack = new Stack<>();
        HashSet<Node> set = new HashSet<>();
        stack.push(node);
        System.out.println(node.v);

        // 栈中保持的是深度优先的路径
        while (!stack.empty()) {
            Node cur = stack.pop();
            for (Node next : cur.nexts) {
                if (!set.contains(next)) {
                    stack.push(cur);
                    stack.push(next);
                    set.add(next);
                    System.out.println(cur.v);
                    break;
                }
            }
        }
    }

    // 常用例子为编译顺序，只适用于有向无环图。
    // 适用场景例如 maven依赖顺序
    // 工程中一般不支持循环依赖
    public List<Node> topologySort(Graph graph) {
        // 先看入度为0的点
        HashMap<Node, Integer> inMap = new HashMap<>();
        Queue<Node> zeroInQueue = new LinkedList<>();
        for (Node value : graph.nodes.values()) {
            if (value.in == 0) {
                zeroInQueue.add(value);
            } else {
                inMap.put(value, value.in);
            }
        }
        List<Node> result = new ArrayList<>();
        while (!zeroInQueue.isEmpty()) {
            Node poll = zeroInQueue.poll();
            result.add(poll);
            for (Node next : poll.nexts) {
                inMap.put(next, inMap.get(next) - 1);
                if (inMap.get(next) == 0) {
                    zeroInQueue.offer(next);
                }
            }
        }
        return result;
    }

    public static class MySets {
        public HashMap<Node, List<Node>> setMap;

        public MySets(Collection<Node> nodes) {
            this.setMap = new HashMap<>(nodes.size());
            for (Node node : nodes) {
                setMap.put(node, Arrays.asList(node));
            }
        }

        public boolean isSameSet(Node from, Node to) {
            return setMap.get(from) == setMap.get(to);
        }

        public void union(Node from, Node to) {
            List<Node> fromSet = setMap.get(from);
            List<Node> toSet = setMap.get(to);
            for (Node node : toSet) {
                fromSet.add(node);
                setMap.put(node, fromSet);
            }
        }
    }
    // 无向图生成最小生成树的两个算法：
    // 最小生成树：没必要要那么多边，只保证直接或间接联通即可，如果有权值，要求全局权值最小
    //K算法：从最小权重的边开始遍历，如果加上会形成换，就不加，反之则加上
    //P算法

    public Set<Edge> k(Graph graph) {
        MySets sets = new MySets(graph.nodes.values());
        PriorityQueue<Edge> edges = new PriorityQueue<>();
        for (Edge edge : graph.edges) {
            edges.add(edge);
        }
        Set<Edge> result = new HashSet<>();
        while (!edges.isEmpty()) {
            Edge poll = edges.poll();
            if (!sets.isSameSet(poll.from, poll.to)) {
                sets.union(poll.from, poll.to);
                result.add(poll);
            }
        }
        return result;
    }

    public Set<Edge> p(Graph graph) {
        Set<Edge> result = new HashSet<>();
        Set<Node> nodeMap = new HashSet<>();
        PriorityQueue<Edge> queue = new PriorityQueue<>();
        for (Node node : graph.nodes.values()) {
            if (!nodeMap.contains(node)) {
                nodeMap.add(node);
                queue.addAll(node.edges);
                while (!queue.isEmpty()) {
                    Edge poll = queue.poll();
                    if (!nodeMap.contains(poll.to)) {
                        result.add(poll);
                        nodeMap.add(poll.to);
                        queue.addAll(poll.to.edges);
                    }
                }
            }
        }
        return result;
    }

    // 迪丽特斯拉算法，单元最短路径算法，权值非负可使用
    public Map<Node, Integer> d(Graph graph, Node start) {
        Map<Node, Integer> distanceMap = new HashMap<>();
        for (Node value : graph.nodes.values()) {
            distanceMap.put(value, Integer.MAX_VALUE);
        }
        HashSet<Node> selected = new HashSet<>();
        distanceMap.put(start, 0);
        selected.add(start);
        Node minNode = start;
        while (minNode != null) {
            Integer distance = distanceMap.get(minNode);
            for (Edge edge : minNode.edges) {
                Node to = edge.to;
                distanceMap.put(to, Math.min(distance + edge.w, distanceMap.get(to)));
            }
            selected.add(minNode);
            minNode = getMinDistanceAndUnSelectedNode(distanceMap, selected);
        }
        return distanceMap;
    }

    // 优化的话用堆
    private Node getMinDistanceAndUnSelectedNode(Map<Node, Integer> distanceMap, HashSet<Node> selected) {
        Node node = null;
        int dis = Integer.MAX_VALUE;
        for (Map.Entry<Node, Integer> entry : distanceMap.entrySet()) {
            if (!selected.contains(entry.getKey())){
                if (entry.getValue()<dis){
                    dis = entry.getValue();
                    node = entry.getKey();
                }
            }
        }
        return node;
    }


}
