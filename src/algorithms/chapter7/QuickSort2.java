package algorithms.chapter7;

/**
 * @Date: 2021/12/23
 * @Author: xuqi
 * @Description:
 */
public class QuickSort2 {
    // 完全实现算法导论上的算法，解决全一样的情况

    public static void quickSort(int[] a, int left, int right) {
        if (left >= right) {
            return;
        }
        // 事先保存了基准值，省去了exchange过程，好聪明
        int x = a[left];
        int i = left;
        int j = right;
        while (j > i) {
            while (j > i && a[j] >= x) {
                j--;
            }
            a[i] = a[j];
            while (j > i && a[i] <= x) {
                i++;
            }
            a[j] = a[i];
        }
        a[i] = x;
        quickSort(a, left, i - 1);
        quickSort(a, i + 1, right);
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 5, 23, 6, 8, 346, 6, 23, 23, 745, 123, 124, 4, 6, 4667, 5};
        quickSort(a, 0, a.length - 1);
        for (int i : a) {
            System.out.println(i);
        }
    }
}
