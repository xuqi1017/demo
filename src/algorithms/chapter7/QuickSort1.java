package algorithms.chapter7;

/**
 * @Date: 2021/12/23
 * @Author: xuqi
 * @Description:
 */
public class QuickSort1 {
    // 完全实现算法导论上的算法，解决全一样的情况

    public static int partition(int[] a, int left, int right) {
        int x = a[right];
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (a[j] <= x) {
                i++;
                exchange(a, i, j);
            }
        }
        exchange(a,i+1,right);
        return i+1;
    }

    private static void exchange(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    public static void quickSort(int[] a, int left, int right) {
        if (left >= right) {
            return;
        }
        int partitionPoint = partition(a, left, right);
        quickSort(a, left, partitionPoint-1);
        quickSort(a, partitionPoint + 1, right);
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 5, 23, 6, 8, 346, 6, 23, 23, 745, 123, 124, 4, 6, 4667, 5};
        quickSort(a, 0, a.length - 1);
        for (int i : a) {
            System.out.println(i);
        }
    }
}
