package algorithms;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @Date: 2021/12/27
 * @Author: xuqi
 * @Description:
 */
public class TreeNode implements Serializable {

    public TreeNode l;
    public TreeNode r;
    public TreeNode p;
    public int v;
    public boolean color;
    public TreeNode() {
    }
    public TreeNode(int v) {
        this.v = v;
    }


    // 用先序
    public static String serializ(TreeNode head) {
        if (head == null) {
            return "#,";
        }
        return head.v + "," + serializ(head.l) + serializ(head.r);
    }

    public static TreeNode revSerializ(String nodes) {
        String[] split = nodes.split(",");
        Queue<String> queue = new LinkedList<>();
        for (String s : split) {
            queue.offer(s);
        }

        return reConPreOrder(queue);
    }

    private static TreeNode reConPreOrder(Queue<String> queue) {
        if (queue.isEmpty()) {
            return null;
        }
        String poll = queue.poll();
        if ("#".equals(poll)) {
            return null;
        }
        TreeNode treeNode = new TreeNode(Integer.parseInt(poll));
        treeNode.l= reConPreOrder(queue);
        treeNode.r= reConPreOrder(queue);
        return treeNode;
    }

}
