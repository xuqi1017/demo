package algorithms.chapter13;

import algorithms.TreeNode;
import algorithms.chapter12.BinarySearchTree;

import java.util.HashMap;

/**
 * 红黑树rbtree
 *
 * @Date: 2021/12/28
 * @Author: xuqi
 * @Description:
 */
public class RedBlackTree extends BinarySearchTree {

    private static final boolean RED = false;
    private static final boolean BLACK = true;
    private static final TreeNode nil = null;


    /**
     * 左旋
     *     x              y
     *    / \            / \
     *   a   y    ->    x   c
     *  / \            / \
     * b   c          a  b
     * 右旋反之
     *
     * @param root
     */
    public void leftRotate(TreeNode root) {
        TreeNode y = root.r;
        root.r = y.l;
        if (y.l != null) {
            y.l.p = root;
        }
        y.p = root.p;
        if (root.p != null) {
            if (root.p.l == root) {
                root.p.l = y;
            } else {
                root.p.r = y;
            }
        }
        y.l = root;
        root.p = y;

    }

    /**
     * 右旋
     *
     * @param root
     */
    public void rightRotate(TreeNode root) {
        TreeNode x = root.l;
        root.l = x.r;
        if (x.r != null) {
            x.r.p = root;
        }
        x.p = root.p;
        if (root.p != null) {
            if (root.p.l == root) {
                root.p.l = x;
            } else {
                root.p.r = x;
            }
        }
        x.r = root;
        root.p = x;
    }

    @Override
    public void insert(int value, TreeNode root) {
        TreeNode newNode = new TreeNode();
        newNode.v = value;
        newNode.color = RED;
        newNode.l = nil;
        newNode.p = nil;
        if (root == null) {
            newNode.p = nil;
            root = newNode;
        }
        TreeNode parent = root;
        while (root != nil) {
            parent = root;
            if (value < root.v) {
                root = root.l;
            } else {
                root = root.r;
            }
        }
        newNode.p = parent;
        if (value > parent.v) {
            parent.r = newNode;
        } else {
            parent.l = newNode;
        }
        insertFixup(newNode);
    }

    private void insertFixup(TreeNode z) {
        while (z.p.color == RED) {
            if (z.p == z.p.p.l) {
                TreeNode y = z.p.p.r;
                if (y.color == RED) {
                    //case 1
                    z.p.color = BLACK;
                    y.color = BLACK;
                    z.p.p.color = RED;
                    z = z.p.p;
                } else {
                    //case 2
                    if (z.p.r == z) {
                        leftRotate(z.p);
                        z = z.l;
                    }
                    //case 3
                    z.p.color = BLACK;
                    z.p.p.color = RED;
                    rightRotate(z.p.p);
                }
            } else {
                TreeNode y = z.p.p.l;
                if (y.color == RED) {
                    y.color = BLACK;
                    z.p.color = BLACK;
                    z.p.p.color = RED;
                    z = z.p.p;
                } else {
                    if (z == z.p.l) {
                        z = z.p;
                        rightRotate(z);
                    }
                    z.p.color = BLACK;
                    z.p.p.color = RED;
                    leftRotate(z.p.p);
                }
            }

        }
    }

    @Override
    public void transplant(TreeNode before, TreeNode after) {
        if (before.p == nil) {
            before = after;
        }
        if (before == before.p.l) {
            before.p.l = after;
        } else {
            before.p.r = after;
        }
        after.p = before.p;
    }
}
