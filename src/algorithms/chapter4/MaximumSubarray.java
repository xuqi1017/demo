package algorithms.chapter4;

/**
 * 三种方法，暴力枚举n^2，分治nlgn ,线性 n
 *
 * @Date: 2021/12/21
 * @Author: xuqi
 * @Description:
 */
public class MaximumSubarray {

    public static int force(int[] a) {
        if (a.length == 0) {
            return 0;
        }
        if (a.length == 1) {
            return a[0];
        }
        int max_sum = 0;
        int low = 0, high = 0;
        for (int i = 0; i < a.length; i++) {
            int sum = 0;
            for (int j = i; j < a.length; j++) {
                sum += a[j];
                if (sum > max_sum) {
                    max_sum = sum;
                    low = i;
                    high = j;
                }
            }
        }
        System.out.println(max_sum);
        // System.out.println(low);
        // System.out.println(high);
        return max_sum;
    }

    public static int line(int[] a) {
        if (a.length == 0) {
            return 0;
        }
        if (a.length == 1) {
            return a[0];
        }
        int max_sum = 0;
        int sum = 0;
        for (int arr : a) {
            sum += arr;
            if (sum < 0) {
                sum = 0;
            }
            if (sum > max_sum) {
                max_sum = sum;
            }
        }
        System.out.println(max_sum);
        return max_sum;
    }


    public static void main(String[] args) {
        int[] a = {1, 2, -5, 23, 6, -8, 346, -6, 23, -23, 745, 123, -124, 4, 6, -4667, 5};
        force(a);
        line(a);
    }

}
