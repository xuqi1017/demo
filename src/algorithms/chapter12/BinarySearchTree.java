package algorithms.chapter12;

import algorithms.TreeNode;
import leetCode.LinkedNode;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 二叉查找树，没有相同的key
 *
 * @Date: 2021/12/28
 * @Author: xuqi
 * @Description:
 */
public class BinarySearchTree {
    /**
     * 遍历中序
     *
     * @param root
     */
    public void inorderWalk1(TreeNode root) {
        if (root == null) {
            return;
        }
        Deque<TreeNode> stack = new ArrayDeque<>();
        while (root != null || !stack.isEmpty()) {
            if (root != null) {
                stack.push(root);
                root = root.l;
            } else {
                root = stack.pop();
                System.out.println(root.v);
                root = root.r;
            }
        }
    }

    /**
     * 递归中序
     *
     * @param root
     */
    public void inorderWalk2(TreeNode root) {
        if (root != null) {
            inorderWalk2(root.r);
            System.out.println(root);
            inorderWalk2(root.l);
        }
    }

    /**
     * 遍历前序
     *
     * @param root
     */
    public void preorderWalk1(TreeNode root) {
        Queue<TreeNode> stack = new LinkedList<>();
        while (root != null || !stack.isEmpty()) {
            if (root != null) {
                System.out.println(root.v);
                stack.add(root);
                root = root.l;
            } else {
                root = stack.poll().r;
            }
        }
    }

    /**
     * 递归前序
     *
     * @param root
     */
    public void preorderWalk2(TreeNode root) {
        if (root != null) {
            System.out.println(root);
            inorderWalk2(root.r);
            inorderWalk2(root.l);
        }
    }

    /**
     * 遍历后序
     *
     * @param root
     */
    public void postorderWalk1(TreeNode root) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        while (root != null || !stack.isEmpty()) {
            if (root != null) {
                stack.push(root);
                root = root.l;
            } else {
                root = stack.pop().r;
                if (root != null) {
                    System.out.println(root.v);
                }
            }
        }
    }

    /**
     * 递归后序
     *
     * @param root
     */
    public void postorderWalk2(TreeNode root) {
        Deque<TreeNode> inStack = new ArrayDeque<>();
        Deque<TreeNode> outStack = new ArrayDeque<>();
        inStack.push(root);
        while (!inStack.isEmpty()) {
            root = inStack.pop();
            outStack.push(root);
            if (root.l != null) {
                inStack.push(root.l);
            }
            if (root.r != null) {
                inStack.push(root.r);
            }
        }
        while (!outStack.isEmpty()) {
            System.out.println(outStack.pop().v);
        }

    }

    /**
     * 层序
     *
     * @param root
     */
    public void levelOrderWalk(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            root = queue.poll();
            System.out.println(root.v);
            if (root.l != null) {
                queue.add(root.l);
            }
            if (root.r != null) {
                queue.add(root.r);
            }
        }
    }

    /**
     * 删节点
     */
    public void delete(int value, TreeNode root) {
        TreeNode search = search(value, root);
        if (search == null) {
            return;
        }
        if (root.l == null) {
            transplant(root, root.r);
        } else if (root.r == null) {
            transplant(root, root.l);
        } else {
            TreeNode successor = successor(root);
            if (root.r != successor) {
                transplant(successor, successor.r);
                successor.r = root.r;
                successor.r.p = successor;
            }
            transplant(root, successor);
            successor.l = root.l;
            successor.l.p = successor;
        }

    }

    public void transplant(TreeNode before, TreeNode after) {
        if (before.p == null) {
            before = after;
        }
        if (before == before.p.l) {
            before.p.l = after;
        } else {
            before.p.r = after;
        }
        if (after != null) {
            after.p = before.p;
        }
    }

    /**
     * 增节点
     */
    public void insert(int value, TreeNode root) {
        TreeNode newNode = new TreeNode();
        newNode.v = value;
        if (root == null) {
            root = newNode;
        }
        TreeNode parent = root;
        while (root != null) {
            parent = root;
            if (value < root.v) {
                root = root.l;
            } else {
                root = root.r;
            }
        }
        newNode.p = parent;
        if (value > parent.v) {
            parent.r = newNode;
        } else {
            parent.l = newNode;
        }
    }

    /**
     * 递归查节点
     */
    public TreeNode search(int value, TreeNode root) {
        if (root == null || value == root.v) {
            return root;
        }
        if (value > root.v) {
            return search(value, root.r);
        } else {
            return search(value, root.l);
        }
    }

    /**
     * 循环查节点
     */
    public TreeNode searchLoop(int value, TreeNode root) {
        while (root != null && value == root.v) {
            if (value > root.v) {
                root = root.r;
            } else {
                root = root.l;
            }
        }
        return root;
    }

    /**
     * 最大子节点
     */
    public TreeNode maximum(TreeNode root) {
        while (root.r != null) {
            root = root.r;
        }
        return root;
    }

    /**
     * 最小子节点
     */
    public TreeNode minimum(TreeNode root) {
        while (root.l != null) {
            root = root.l;
        }
        return root;
    }

    /**
     * 后继节点
     */
    public TreeNode successor(TreeNode root) {
        if (root.r != null) {
            while (root.l != null) {
                root = root.l;
            }
            return root;
        }
        TreeNode parent = root.p;
        while (parent != null && parent.r == root) {
            root = parent;
            parent = parent.p;
        }
        return parent;
    }

    /**
     * 前驱节点
     */
    public TreeNode predecessor(TreeNode root) {
        if (root.l != null) {
            return maximum(root.l);
        }
        TreeNode parent = root.p;
        while (parent != null && parent.l == root) {
            root = parent;
            parent = parent.p;
        }
        return parent;
    }

    /**
     * 判断是否是二叉搜索树，是否是完全二叉树，是否是满二叉树，是否是平衡二叉树
     */
    public boolean checkBinarySearch(TreeNode head) {
        //中序遍历 是从小到大的
        if (head == null) {
            return true;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(head);
        int val = Integer.MIN_VALUE;
        while (head == null || !stack.isEmpty()) {
            if (head != null) {
                stack.push(head);
                head = head.l;
            } else {
                head = stack.pop();
                if (head.v < val) {
                    return false;
                }
                val = head.v;
                head = head.r;
            }
        }
        return true;
    }

    public boolean checkBinaryComplete(TreeNode head) {
        // 层序遍历二叉树，计算宽度 .补充：完全二叉树，除了最后一层，其余层都是满的
        // 1任何一个节点都得是2在遇到一个左右孩子不全的节点，之后的节点必为叶子节点
        if (head == null) {
            return true;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(head);
        boolean leaf = false;
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            System.out.println(poll);
            if ((leaf && (poll.l != null || poll.r != null)) || (poll.l == null && poll.r != null)) {
                return false;
            }
            if (poll.l != null) {
                queue.offer(poll.l);
            }
            if (poll.r != null) {
                queue.offer(poll.r);
            }
            if (poll.l == null || poll.r == null) {
                leaf = true;
            }
        }
        return true;
    }


    class CheckBinaryFullData {
        int hight;
        int number;

        CheckBinaryFullData(int i, int j) {
            hight = i;
            number = j;
        }
    }

    boolean checkBinaryFullMain(TreeNode head) {
        CheckBinaryFullData data = checkBinaryFull(head);
        return data.number == 1 << data.hight - 1;
    }

    public CheckBinaryFullData checkBinaryFull(TreeNode head) {
        // 可以先求深度和节点个数，满足 N=2^l - 1
        if (head == null) {
            return new CheckBinaryFullData(0, 0);
        }
        CheckBinaryFullData leftData = checkBinaryFull(head.l);

        CheckBinaryFullData rightData = checkBinaryFull(head.r);

        return new CheckBinaryFullData(Math.max(leftData.hight, rightData.hight) + 1, leftData.number + leftData.number + 1);
    }

    // 平衡二叉树：任何一个树，坐树和右树高度差不超过1
    public int checkBinaryBalance(TreeNode head) {
        if (head == null) {
            return 0;
        }
        int lh = checkBinaryBalance(head.l);
        int rh = checkBinaryBalance(head.r);
        if (lh == -1 || rh == -1) {
            return -1;
        }
        if (Math.abs(lh - rh) > 1) {
            return -1;
        }
        return Math.max(lh, rh) + 1;
    }
}
