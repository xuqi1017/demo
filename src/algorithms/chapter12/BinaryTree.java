package algorithms.chapter12;

import algorithms.TreeNode;

import java.util.*;

/**
 * @Date: 2023/11/24
 * @Author: xuqi
 * @Description:
 */
public class BinaryTree {
    /**
     * 找到两个节点的最低公共父节点
     */
    public TreeNode publicParentNode(TreeNode head, TreeNode node1, TreeNode node2) {
        if (head == null || node1 == head || node2 == head) {
            return head;
        }
        TreeNode l = publicParentNode(head.l, node1, node2);
        TreeNode r = publicParentNode(head.r, node1, node2);
        if (l != null && r != null) {
            return head;
        }

        return l == null ? r : l;
    }

    public static void zheZhi(int n) {
        zheZhiPrint(n, 1, true);
    }

    public static void main(String[] args) {
        zheZhi(10);
    }

    private static void zheZhiPrint(int n, int i, boolean b) {
        if (i > n) {
            return;
        }
        zheZhiPrint(n, i + 1, true);
        System.out.println(b ? "凹" : "凸");
        zheZhiPrint(n, i + 1, false);
    }

    public static List<Integer> rightView(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Map<Integer, Integer> depthResult = new HashMap<>();
        Queue<TreeNode> queue = new LinkedList<>();
        Queue<Integer> depth = new LinkedList<>();

        queue.add(root);
        depth.add(0);
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            Integer cur_depth = depth.poll();
            depthResult.put(cur_depth, poll.v);
            if (poll.l != null) {
                queue.add(poll.l);
                depth.add(cur_depth + 1);
            }
            if (poll.r != null) {
                queue.add(poll.r);
                depth.add(cur_depth + 1);
            }
        }
        for (int i = 0; ; i++) {
            if (depthResult.get(i) == null) {
                break;
            }
            result.add(depthResult.get(i));
        }
        return result;
    }


    public static List<Integer> leftView(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Map<Integer, Integer> depthResult = new HashMap<>();
        Queue<TreeNode> queue = new LinkedList<>();
        Queue<Integer> depth = new LinkedList<>();

        queue.add(root);
        depth.add(0);
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            Integer cur_depth = depth.poll();
            if (!depthResult.containsKey(cur_depth)){
                depthResult.put(cur_depth, poll.v);
            }
            if (poll.l != null) {
                queue.add(poll.l);
                depth.add(cur_depth + 1);
            }
            if (poll.r != null) {
                queue.add(poll.r);
                depth.add(cur_depth + 1);
            }
        }
        for (int i = 0; ; i++) {
            if (depthResult.get(i) == null) {
                break;
            }
            result.add(depthResult.get(i));
        }
        return result;
    }

    public static int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.min(minDepth(root.l), minDepth(root.r)) + 1;
    }
}
