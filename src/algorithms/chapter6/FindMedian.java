package algorithms.chapter6;

import java.util.PriorityQueue;

/**
 * 一个流数据，实时获取中位数，用两个堆优化
 *
 * @Date: 2023/12/4
 * @Author: xuqi
 * @Description:
 */
public class FindMedian {
    //存小数
    PriorityQueue<Integer> maxHeap;
    //存大数
    PriorityQueue<Integer> minHeap;

    FindMedian() {
        maxHeap = new PriorityQueue<>();
        minHeap = new PriorityQueue<>();
    }

    public int getMedian() {
        if (maxHeap.size() > minHeap.size()) {
            return maxHeap.peek();
        } else if (maxHeap.size() < minHeap.size()) {
            return minHeap.peek();
        } else {
            return (maxHeap.peek() + minHeap.peek()) / 2;
        }
    }

    public void input(int i) {
        if (maxHeap.isEmpty()) {
            maxHeap.add(i);
            return;
        }
        if (i <= maxHeap.peek()) {
            maxHeap.add(i);
        } else {
            minHeap.add(i);
        }
        if (maxHeap.size()-minHeap.size()==2){
            minHeap.add(maxHeap.poll());
        }else if (minHeap.size()-maxHeap.size()==2){
            maxHeap.add(minHeap.poll());
        }
    }
}
