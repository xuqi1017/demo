package algorithms.chapter6;

import java.util.PriorityQueue;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * 堆分为大根堆和小根堆
 *
 * @Date: 2021/12/24
 * @Author: xuqi
 * @Description:
 */
public class HeapSort {
    public static int left(int i) {
        return 2 * i;
    }

    public static int right(int i) {
        return 2 * i + 1;
    }

    public static int parent(int i) {
        return i / 2;
    }


    public static void heapSort(int[] a) {
        int heapSize = a.length;
        buildMaxHeap(a, heapSize);

        for (int i = a.length - 1; i > 0; i--) {
            heapSize--;
            exchange(a, 0, i);
            maxHeapify(a, 0, heapSize);
        }
    }

    private static void exchange(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    public static void buildMaxHeap(int[] a, int heapSize) {
        for (int i = a.length / 2; i >= 0; i--) {
            maxHeapify(a, i, heapSize);
        }
    }

    public static void maxHeapInsert(int[] a, int i) {
        int parent = (i + 1) / 2;
        while (a[parent] < a[i]) {
            exchange(a, i, parent);
            i = parent;
        }
    }

    public static void maxHeapify(int[] a, int i, int heapSize) {
        int left = left(i);
        int right = right(i);
        int largest = i;
        if (left < heapSize && a[left] > a[i]) {
            largest = left;
        }
        if (right < heapSize && a[right] > a[largest]) {
            largest = right;
        }
        if (largest != i) {
            exchange(a, i, largest);
            maxHeapify(a, largest, heapSize);
        }
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 5, 23, 6, 8, 346, 6, 23, 23, 745, 123, 124, 4, 6, 4667, 5};
        heapSort(a);
        for (int i : a) {
            System.out.println(i);
        }

        int i = -1 / 2;
        System.out.println(i);
    }

    /**
     * 一个几乎有序的数组排序，一个数字离它原有位置不超过k
     * @param a
     */
    public void heapSortSorted(int[] a ,int k){
        // 小根堆
        // 有些面试题需要手写堆，系统不提供修改值的接口
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        for (int i = 0; i <= k; i++) {
            heap.add(a[i]);
        }
        int j=0;
        for (int i = k; i < a.length-k; i++) {
            a[j++]=heap.poll();
            heap.add(a[i]);
        }
        for (int i = 0; i < k; i++) {
            a[j++]=heap.poll();
        }
    }
}
