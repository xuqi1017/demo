package algorithms.chapter18;

import java.util.HashMap;
import java.util.Map;

public class TrieTree {

    TrieNode head;

    public static class TrieNode {
        int pass;
        int end;
        Map<Character, TrieNode> next;

        public TrieNode() {
            pass = 0;
            end = 0;
            next = new HashMap<>();
        }
    }

    public static void insert(TrieNode head, String s) {
        if (s == null || s.length() == 0) {
            return;
        }
        head.pass++;
        for (int i = 0; i < s.toCharArray().length; i++) {
            TrieNode trieNode = head.next.get(s.charAt(i));
            if (trieNode == null) {
                trieNode = new TrieNode();
                head.next.put(s.charAt(i), trieNode);
            }
            trieNode.pass++;
            head = trieNode;
        }
        head.end++;
    }

    public static void delete(TrieNode head,String s){
        if (search(head,s)==0){
            return;
        }
        head.pass--;
        for (int i = 0; i < s.toCharArray().length; i++) {
            TrieNode head1 = head.next.get(s.charAt(i));
            if (--head1.pass==0){
                head.next.put(s.charAt(i),null);
            }
            head = head1;
        }
        head.end--;
    }

    public static int starWith(TrieNode head, String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        for (int i = 0; i < s.toCharArray().length; i++) {
            head = head.next.get(s.charAt(i));
            if (head == null) {
                return 0;
            }
        }
        return head.pass ;
    }

    public static int search(TrieNode head, String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        for (int i = 0; i < s.toCharArray().length; i++) {
            head = head.next.get(s.charAt(i));
            if (head == null) {
                return 0;
            }
        }
        return head.end ;
    }

    public static void main(String[] args) {
        TrieNode head = new TrieNode();
        insert(head, "asf");
        insert(head, "asw");
        insert(head, "asa");
        insert(head, "bsf");
        System.out.println();
    }
}
