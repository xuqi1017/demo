package algorithms.chapter18;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @Date: 2023/6/7
 * @Author: xuqi
 * @Description:
 */
public class BTreeNode {

    /**
     * 最小度数 的固定整数，minimum degree
     */
    private int t;
    /**
     * 关键字
     */
    private List<Integer> keys;
    /**
     * 子节点指针
     */
    private List<BTreeNode> children;
    /**
     * 是否叶子节点
     */
    private boolean leaf;
    /**
     * 是否根节点
     */
    private boolean root;

    private BTreeNode(int t, int val) {
        this.t = t;
        this.leaf = true;
        this.keys = new ArrayList<>();
        this.children = new ArrayList<>();
    }

    public static BTreeNode createRoot(int t, int val) {
        BTreeNode root = new BTreeNode(t, val);
        root.root = true;
        root.keys.add(val);
        return root;
    }

    public Integer search(int key) {
        int i = 0;
        int size = this.keys.size();
        while (i < size && key < this.keys.get(i)) {
            i++;
        }
        if (i < size && key == this.keys.get(i)) {
            return this.keys.get(i);
        } else if (this.leaf) {
            return null;
        }
        return this.children.get(i).search(key);
    }

    public void insert(int key) {
        for (int i = 0; i < keys.size(); i++) {
            if (key < keys.get(i)) {
                // 应该插入第 i+1 个关键词的左子树
                BTreeNode bTreeNode = this.children.get(i);
                bTreeNode.insert(key, this);
                return;
            }
        }
        this.children.get(keys.size()).insert(key, this);
    }
    //TODO 及时调整 leaf 字段

    public void insert(int key, BTreeNode parent) {
        if (isFull()) {
            this.split(parent);
        }
        doInsert(key,null);

    }

    private boolean isFull() {
        // 最大的关键字数量为 2t-1
        return this.keys.size() == 2 * this.t - 1;
    }

    public void split(BTreeNode parent) {
        // 此节点必是满的，也就是 key的size是2t-1
        // 进行分裂的关键字 一定是最中间的那个，也就是第t个，index也就是t-1
        // 新的关键字是从parent上下来的，所以parent必不满
        Integer key = this.keys.get(t - 1);
        // 新建一个节点作为右节点
        BTreeNode rightNode = new BTreeNode(t,key);
        rightNode.leaf = this.leaf;
        int size = this.keys.size();
        for (int i = t; i < size; i++) {
            rightNode.keys.add(this.keys.get(i));
        }
        for (int i = t; i < size; i++) {
            this.keys.remove(i);
        }
        for (int i = t+1; i <= size; i++) {
            rightNode.children.add(this.children.get(i));
        }
        for (int i = t+1; i <= size; i++) {
            this.children.remove(i);
        }
        // TODO 把key存到parent;
        parent.doInsert(key,rightNode);
    }

    public void print(){
        // 递归的打印
        if (this.children.size()==0){
            return;
        }
        for (int i = 0; i < this.keys.size(); i++) {
            this.children.get(i).print();
            System.out.println(this.keys.get(i));;
        }
        this.children.get(this.keys.size()).print();
    }

    private void doInsert(int key,BTreeNode right){
        int size = keys.size();
        for (int i = 0; i <size; i++) {
            if (key < keys.get(i)) {
                // 应该插入第 i+1 个关键词的左子树
                this.keys.add(i,key);
                this.children.add(i+1,right);
                return;
            }
        }
        this.keys.add(size,key);
        this.children.add(size+1,right);
    }

    public static void main(String[] args) {
        BTreeNode root = createRoot(4, 500);
        //增 删 查
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            root.insert(random.nextInt(1000));
        }
        root.print();
    }
}
